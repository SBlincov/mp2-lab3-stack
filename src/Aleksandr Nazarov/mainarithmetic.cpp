#include "arithmetic.h"

using namespace std;

int main()
{
	arithmetic exp;
	string expression;
	cout << "write expression: ";
	getline(cin,expression);
	while (expression != "exit")
	{
		try
		{
			exp.PutInfix(expression);
			cout << "result is " << exp.GetResult() << endl;
			cout << "write expression: ";
			expression.clear();
			getline(cin, expression);
		}
		catch (char *err)
		{
			cout << err << endl;
			cout << "write expression: ";
			expression.clear();
			getline(cin, expression);
		}
	}
}