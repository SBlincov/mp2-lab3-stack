
#include "TSimpleStack.h"

int main()
{
	TSimpleStack<int> stack1, stack2;
	for (int i = 0; i < 5; i++)
	{
		stack1.Put(i);
		stack2.Put(i*i);
	}
	cout << "stack1 :" << stack1 << endl;
	cout << "stack2 :" << stack2 << endl;
	stack1.Swap(stack2);
	cout << "Swap: " << endl;
	cout << "stack1 :" << stack1 << endl;
	cout << "stack2 :" << stack2 << endl;
	system("pause");
}