# Методы программирования 2: Стек

## Цели и задачи

В рамках лабораторной работы ставится задача разработки двух видов стеков:

- простейшего, основанного на статическом массиве (класс TSimpleStack);
- более сложного, основанного на использовании динамической структуры (класс TStack).

С помощью разработанных стеков необходимо написать приложение, которое вычисляет арифметическое выражение, заданное в виде строки и вводится пользователем. Сложность выражения ограничена только длиной строки.

В процессе выполнения лабораторной работы требуется использовать систему контроля версий Git и фрэймворк для разработки автоматических тестов Google Test.

Перед выполнением работы студенты получают данный проект-шаблон, содержащий следующее:

- Интерфейсы классов TDataCom и TDataRoot (h-файлы)
- Тестовый пример использования класса TStack

Выполнение работы предполагает решение следующих задач:

- Разработка класса TSimpleStack на основе массива фиксированной длины.
- Реализация методов класса TDataRoot согласно заданному интерфейсу.
- Разработка класса TStack, являющегося производным классом от TDataRoot.
- Разработка тестов для проверки работоспособности стеков.
- Реализация алгоритма проверки правильности введенного арифметического выражения.
- Реализация алгоритмов разбора и вычисления арифметического выражения.
- Обеспечение работоспособности тестов и примера использования.

## Разработка класса TSimpleStack на основе массива фиксированной длины

```C++
//  Стек фиксированной длинны на шаблоне

#include <iostream>

#define DefStackSize 50									//размер стека

using namespace std;

template <class ValType>
class TSimpleStack
{
protected:                                   
	int p;                                              // указатель на последний элемент
	ValType pStack[DefStackSize];                               // память стека
public:
	TSimpleStack() { p = 0; }
	TSimpleStack(const TSimpleStack<ValType> &s);  //конструкстор копирования
	bool IsEmpty() { return (p == 0); }            //проверка пустоты
	bool IsFull() { return (p == DefStackSize); }          //проверка полноты
	int GetP() { return p; }                       //вернуть указатель на последний элемент
	void Put(ValType val);                         //вставить элемент
	ValType Get();                                 //взять элемент
	void pop_back() { p--; }                       //удалить последний элемент
	friend ostream& operator<<(ostream &out, const TSimpleStack &s) //вывод
	{
		for (int i = 0; i < s.p; i++)
			out << s.pStack[i] << " ";
		return out;
	}
};

template <class ValType>
TSimpleStack<ValType>::TSimpleStack(const TSimpleStack<ValType> &s)
{
	p = s.p;
	for (int i = 0; i < DefStackSize; i++)
		pStack[i] = s.pStack[i];
}

template <class ValType>
void TSimpleStack<ValType>::Put(ValType val)
{
	if (IsFull()) throw "stack overflow";
	pStack[p++] = val;
}

template <class ValType>
ValType TSimpleStack<ValType>::Get()
{
	if (IsEmpty()) throw "stack is blank";
	return pStack[--p];
}
```

## Реализация методов класса TDataRoot согласно заданному интерфейсу

```C++
#ifndef __DATAROOT_H__
#define __DATAROOT_H__

#include "tdatacom.h"
#include <iostream>

using namespace std;

#define DefMemSize   25  // размер памяти по умолчанию

#define DataEmpty  -101  // СД пуста
#define DataFull   -102  // СД переполнена
#define DataNoMem  -103  // нет памяти

enum TMemType { MEM_HOLDER, MEM_RENTER };

template <class ValType>
class TDataRoot : public TDataCom
{
protected:
	ValType* pMem;      // память для СД
	int MemSize;      // размер памяти для СД
	int DataCount;    // количество элементов в СД
	TMemType MemType; // режим управления памятью

	void SetMem(void *p, int Size);             // задание памяти
public:
	virtual ~TDataRoot();
	TDataRoot(int Size = DefMemSize);
	virtual bool IsEmpty(void) const;           // контроль пустоты СД
	virtual bool IsFull(void) const;           // контроль переполнения СД
	virtual void  Put(const ValType &Val) = 0; // добавить значение
	virtual ValType Get(void) = 0; // извлечь значение

								 // служебные методы
	virtual int  IsValid() = 0;                 // тестирование структуры
	virtual void Print() = 0;                 // печать значений

											  // дружественные классы
	friend class TMultiStack;
	friend class TSuperMultiStack;
	friend class TComplexMultiStack;
};

template <class ValType>
void TDataRoot<ValType>::SetMem(void *p, int Size) // задание памяти
{
	if (Size <= 0) throw "wrong size to setmem";
	if (MemType != MEM_RENTER)
	{
		MemType = MEM_RENTER;
		delete[] pMem;
	}
	pMem = (TElem *)p;
	MemSize = Size;
}

template <class ValType>
TDataRoot<ValType>::TDataRoot(int Size) : TDataCom()
{
	if (Size == 0)
	{
		MemType = MEM_RENTER;
		pMem = nullptr;
		MemSize = 0;
	}
	else
	{
		MemType = MEM_HOLDER;
		if (Size < 0) throw "wrong size of data";
		DataCount = 0;
		MemSize = Size;
		pMem = nullptr;
		pMem = new ValType[Size];
		if (pMem == nullptr) throw "not enough memory to build data";
		memset(pMem, 0, sizeof(ValType)*MemSize);
	}
}

template <class ValType>
TDataRoot<ValType>::~TDataRoot()
{
	if (MemType == MEM_HOLDER) delete[] pMem;
	else pMem = nullptr;
}

template <class ValType>
bool TDataRoot<ValType>::IsEmpty(void) const   // контроль пустоты СД
{
	return DataCount == 0;
}

template <class ValType>
bool TDataRoot<ValType>::IsFull(void) const    // контроль переполнения СД
{
	return DataCount == MemSize;
}


#endif
```

##Разработка класса TStack, являющегося производным классом от TDataRoot

```C++
#include "tdataroot.h"
#include "TSimpleStack.h"
#include <iostream>

#define STDDATASETMEM 25

using namespace std;

template <class ValType>
class TStack : public TDataRoot<ValType>
{
protected:
	void Reconstruction(int Size);
public:
	TStack(int Size = DefMemSize) : TDataRoot(Size) {};
	TStack(const TStack &s);          // конструктор копирования
	void Put(const ValType &val) { if (IsFull()) Reconstruction(STDDATASETMEM); pMem[DataCount++] = val; }; // добавить элемент
	ValType Get() { return (IsEmpty()) ? (throw SetRetCode(DataEmpty)) : pMem[--DataCount]; };   // извлечь элемент
	void pop_back() { DataCount-- };
	int IsValid();              // тестирование структуры 
	void Print();               // печать значений
};


template <class ValType>
TStack<ValType>::TStack(const TStack &s) : TDataRoot(s.Size), Top(s.Top)
{
	for (int i = 0; i < MemSize; i++)
		pMem[i] = s.pMem[i];
}

template <class ValType>
void TStack<ValType>::Reconstruction(int Size)
{
	if (MemType == MEM_HOLDER)
	{
		ValType *Temp;
		Temp = new ValType[MemSize + Size];
		for (int i = 0; i < MemSize; i++)
			Temp[i] = pMem[i];
		delete[] pMem;
		pMem = Temp;
		MemSize += Size;
	}
	else
		throw SetRetCode(DataFull);
}

template <class ValType>
int TStack<ValType>::IsValid()
{
	if (pMem == nullptr || MemSize < DataCount || DataCount < 0 || MemSize < 0) 
		return 0;
	return 1;
}

template <class ValType>
void TStack<ValType>::Print()
{
	for (int i = 0; i < DataCount; i++)
		cout << pMem[i] << " ";
	cout << endl;
}
```

##Разработка тестов для проверки работоспособности стеков
###Тесты для TSimpleStack

```C++
#include "gtest.h"
#include "TSimpleStack.h"

TEST(TSimpleStack, can_create_simple_stack_with_positive_length)
{
	ASSERT_NO_THROW(TSimpleStack<int> A);
}
TEST(TSimpleStack, can_put_any_value_to_simple_stack)
{
	TSimpleStack<int> A;
	TSimpleStack<char> B;
	ASSERT_NO_THROW(A.Put(1));
	ASSERT_NO_THROW(B.Put('C'));
}
TEST(TSimpleStack, can_get_any_value_from_stack)
{
	TSimpleStack<int> A;
	TSimpleStack<char> B;
	A.Put(1);
	B.Put('C');
	EXPECT_EQ(1, A.Get());
	EXPECT_EQ('C', B.Get());
	EXPECT_EQ(1, A.IsEmpty());
}
TEST(TSimpleStack, cant_put_value_to_overflow_simple_stack)
{
	TSimpleStack<int> A;
	for (int i = 0; i < DefStackSize; i++)
		A.Put(2);
	ASSERT_ANY_THROW(A.Put(2));
}
TEST(TSimpleStack, cant_get_any_value_from_empty_simple_stack)
{
	TSimpleStack<int> A;
	ASSERT_ANY_THROW(A.Get());
}
TEST(TSimpleStack, can_copied_simple_stack)
{
	TSimpleStack<int> A;
	A.Put(100);
	TSimpleStack<int> B(A);
	EXPECT_EQ(100, B.Get());
	EXPECT_EQ(0, A.IsEmpty());
}

```
###Тесты для TStack

```C++
#include "gtest.h"
#include "TStack.h"

TEST(TStack, can_create_stack_with_positive_length)
{
	ASSERT_NO_THROW(TStack<int> a(5));
}
TEST(TStack, cant_create_stack_with_negative_length)
{
	ASSERT_ANY_THROW(TStack<int> a(-5));
}
TEST(TStack, function_IsFull_return_right_value)
{
	TStack<int> a(1);
	EXPECT_EQ(0, a.IsFull());
	a.Put(1);
	EXPECT_EQ(1, a.IsFull());
}
TEST(TStack, function_IsEmpty_return_right_value)
{
	TStack<int> a(1);
	EXPECT_EQ(1, a.IsEmpty());
	a.Put(1);
	EXPECT_EQ(0, a.IsEmpty());
}
TEST(TStack, can_put_any_value_into_stack)
{
	TStack<int> a(2);
	ASSERT_NO_THROW(a.Put(5));
	ASSERT_NO_THROW(a.Put(2));
	EXPECT_EQ(0, a.IsEmpty());
}
TEST(TStack, can_get_any_value_from_stack)
{
	TStack<int> a(2);
	a.Put(5);
	a.Put(2);
	EXPECT_EQ(2, a.Get());
	EXPECT_EQ(5, a.Get());
	EXPECT_EQ(1, a.IsEmpty());
}

```

##Реализация алгоритма проверки правильности введенного арифметического выражения

```C++
bool arithmetic::IsValid(string str)
{
	bool flag = true;
	int open = 0, close = 0;
	for (int i = 0; (i < str.length()) && flag; i++)
	{
		if (str.at(i) == '(') open++;
		if (str.at(i) == ')') close++;
		if (close > open) flag = false;
	}
	if (close != open) flag = false;
	if (!flag)
	{
		TStack<int> st;
		int numOfErrors = 0, bracketNum = 0;
		cout << "Brackets" << endl;
		cout << "Opening" << " " << "Closing" << endl;
		for (int i = 0; i < str.length(); i++)
		{
			if (str.at(i) == '(')
				 st.Put(++bracketNum);
			if (str.at(i) == ')')
			{
				bracketNum++;
				if (st.IsEmpty())
				{
					numOfErrors++;
					cout << '-' << "       " << bracketNum << endl;
				}
				else
				cout << st.Get() << "       " << bracketNum << endl;
			}
		}
		while (!st.IsEmpty())
		{
			cout << st.Get() << "       " << '-' << endl;
			numOfErrors++;
		}
		cout << "Number of errors: " << numOfErrors << endl;
	}
	return flag;
}
```
##Реализация алгоритмов разбора и вычисления арифметического выражения

```C++
string arithmetic::InfToPost(string str)
{
	TStack<char> Symbols;
	string post;
	char Buffer;
	int LastPriority = 0, Prrt;
	for (int i = 0; i < str.length(); i++)
	{
		Prrt = PriorityOfChar(str.at(i));
		if (Prrt == 0 || Prrt > LastPriority || (Symbols.IsEmpty() && Prrt != -1))
		{
			Symbols.Put(str.at(i));
			LastPriority = Prrt;
		}
		else if (Prrt != -1)
		{
			if (Prrt == 1)
			{
				Buffer = Symbols.Get();
				while (PriorityOfChar(Buffer) != 0)
				{
					post.push_back(Buffer);
					Buffer = Symbols.Get();
				}
			}
			else
			{
				Buffer = Symbols.Get();
				while (PriorityOfChar(Buffer) >= Prrt && !Symbols.IsEmpty())
				{
					post.push_back(Buffer);
					Buffer = Symbols.Get();
				}
				if (!Symbols.IsEmpty() || PriorityOfChar(Buffer) < Prrt)
					Symbols.Put(Buffer);
				Symbols.Put(str.at(i));
				LastPriority = Prrt;
			}
		}
		if (Prrt == -1)
			post.push_back(str.at(i));
		if (Prrt != -1)
			post.push_back(' ');

	}
	while (!Symbols.IsEmpty()) post.push_back(Symbols.Get());
	return post;
}

int arithmetic::PriorityOfChar(char ch)
{
	for (int i = 0; i < NumOfPriority; i++)
	{
		for (int f = 0; f < priority[i].length(); f++)
			if (priority[i].at(f) == ch) return i;
	}
	return -1;
}

double arithmetic::PostToResult(string str)
{
	TStack<double> res;
	string strtoval;
	double oper1, oper2;
	for (int i = 0; i < str.length(); i++)
	{
		while (PriorityOfChar(str.at(i)) == -1 && str.at(i) != ' ' &&  i < str.length())
			strtoval.push_back(str.at(i++));
		if (!strtoval.empty())
		{
			res.Put(StrToDouble(strtoval));
			strtoval.clear();
		}
		if (PriorityOfChar(str.at(i)) != -1)
		{
			oper2 = res.Get();
			oper1 = res.Get();
			res.Put(Operations(oper1, oper2, str.at(i)));
		}
	}
	return ((res.IsEmpty()) ? 0 : res.Get());
}

double arithmetic::Operations(double num1, double num2, char operation)
{
	switch (operation)
	{
	case '/':
	{
		return num1 / num2;
		break;
	}
	case '*':
	{
		return num1 * num2;
		break;
	}
	case '+':
	{
		return num1 + num2;
		break;
	}
	case '-':
	{
		return num1 - num2;
		break;
	}
	default: 
		throw "unknow operation";
	}
}

double arithmetic::StrToDouble(string str)
{
	double result = 0;
	int i;
	bool point = false;
	for (i = 0; i < str.length(); i++)
	{
		if ((str.at(i) == '.') && point) throw "wrong value";
		if (str.at(i) == '.') point = true;
	}
	if (point)
	{
		for (int p = 1, i = (str.find('.') + 1); i < str.length(); i++, p++)
		{
			if (str.at(i) <= '9' && str.at(i) >= '0')
				result += (str.at(i) - '0') * pow(10, -p);
			else
				throw "wrong value";
		}
		for (int p = 0, i = (str.find('.') - 1); i >= 0; i--, p++)
		{
			if (str.at(i) <= '9' && str.at(i) >= '0')
				result += (str.at(i) - '0') * pow(10, p);
			else
				throw "wrong value";
		}
	}
	else
		for (int p = 0, i = str.length() - 1; i >= 0; i--, p++)
		{
			if (str.at(i) <= '9' && str.at(i) >= '0')
				result += (str.at(i) - '0') * pow(10, p);
			else
				throw "wrong value";
		}
	return result;
}

```

##Обеспечение работоспособности тестов и примера использования
###Примеры использования

![Expression](http://i.imgur.com/SZ15J8O.png)

![IsValid](http://i.imgur.com/e18Hii5.png)

![Main](http://i.imgur.com/lCmeGKM.png)

###Тесты

![Tests](http://i.imgur.com/CKE3jQd.png)

##Вывод

Успешно выполнены все поставленные задачи.
Разработаны и протестированы классы TStack и TSimpleStack, реализованы алгоритмы разбора и вычисления арифметического выражения. Отработаны навыки работы с шаблонами и с google tests. 

##Приложение

###Полная версия класса arithmetic

####Файл arithmetic.h

```C++
#include "TStack.h"
#include <string>
#include <iostream>

#define NumOfPriority 4

using namespace std;

class arithmetic
{
protected:
	string postfix, infix;                                   // память для хранения постфиксной и инфиксной формы записи
	double result;											// память для хранения результата
	string priority[NumOfPriority] = { "(",")","+-","*/" }; // операции и их приоритет
	string InfToPost(string str);							// метод перевода из инфиксной в постфиксную форому
	double PostToResult(string str);						// метод расчета результата из постфиксной формы
	double StrToDouble(string str);							// перевод из string в double
	bool IsValid(string str);								// проверка правильности ввода скобок
	int PriorityOfChar(char ch);							// вывод приоритета данного символа
	double Operations(double num1, double num2, char operation);
public:
	arithmetic(string str = "");
	void PutInfix(string str);
	string GetPostfix() { return postfix; };
	string GetInfix() { return infix; };
	double GetResult() { return result; };
	void PrintInfix() { cout << infix << endl; };
	void PrintPostfix() { cout << postfix << endl; };
	void PrintResult() { cout << result << endl; };
	friend ostream& operator<<(ostream &out, const arithmetic &a)
	{
		out << "postfix: " << a.postfix << endl;
		out << "infix: " << a.infix << endl;
		out << "result: " << a.result << endl;
		return out;
	}
};
```
####Файл arithmetic.cpp

```C++

#include "arithmetic.h"


arithmetic::arithmetic(string str)
{
	if (IsValid(str))
	{
		infix = str;
		postfix = InfToPost(infix);
		result = (str.size()) ? PostToResult(postfix) : 0;
	}
	else
		throw "wrong string";
}

void arithmetic::PutInfix(string str)
{
	if (IsValid(str))
	{
		infix = str;
		postfix = InfToPost(infix);
		result = (str.size()) ? PostToResult(postfix) : 0;
	}
	else
		throw "wrong string";
}

string arithmetic::InfToPost(string str)
{
	TStack<char> Symbols;
	string post;
	char Buffer;
	int LastPriority = 0, Prrt;
	for (int i = 0; i < str.length(); i++)
	{
		Prrt = PriorityOfChar(str.at(i));
		if (Prrt == 0 || Prrt > LastPriority || (Symbols.IsEmpty() && Prrt != -1))
		{
			Symbols.Put(str.at(i));
			LastPriority = Prrt;
		}
		else if (Prrt != -1)
		{
			if (Prrt == 1)
			{
				Buffer = Symbols.Get();
				while (PriorityOfChar(Buffer) != 0)
				{
					post.push_back(Buffer);
					Buffer = Symbols.Get();
				}
			}
			else
			{
				Buffer = Symbols.Get();
				while (PriorityOfChar(Buffer) >= Prrt && !Symbols.IsEmpty())
				{
					post.push_back(Buffer);
					Buffer = Symbols.Get();
				}
				if (!Symbols.IsEmpty() || PriorityOfChar(Buffer) < Prrt)
					Symbols.Put(Buffer);
				Symbols.Put(str.at(i));
				LastPriority = Prrt;
			}
		}
		if (Prrt == -1)
			post.push_back(str.at(i));
		if (Prrt != -1)
			post.push_back(' ');

	}
	while (!Symbols.IsEmpty()) post.push_back(Symbols.Get());
	return post;
}

int arithmetic::PriorityOfChar(char ch)
{
	for (int i = 0; i < NumOfPriority; i++)
	{
		for (int f = 0; f < priority[i].length(); f++)
			if (priority[i].at(f) == ch) return i;
	}
	return -1;
}

double arithmetic::PostToResult(string str)
{
	TStack<double> res;
	string strtoval;
	double oper1, oper2;
	for (int i = 0; i < str.length(); i++)
	{
		while (PriorityOfChar(str.at(i)) == -1 && str.at(i) != ' ' &&  i < str.length())
			strtoval.push_back(str.at(i++));
		if (!strtoval.empty())
		{
			res.Put(StrToDouble(strtoval));
			strtoval.clear();
		}
		if (PriorityOfChar(str.at(i)) != -1)
		{
			oper2 = res.Get();
			oper1 = res.Get();
			res.Put(Operations(oper1, oper2, str.at(i)));
		}
	}
	return ((res.IsEmpty()) ? 0 : res.Get());
}

double arithmetic::Operations(double num1, double num2, char operation)
{
	switch (operation)
	{
	case '/':
	{
		return num1 / num2;
		break;
	}
	case '*':
	{
		return num1 * num2;
		break;
	}
	case '+':
	{
		return num1 + num2;
		break;
	}
	case '-':
	{
		return num1 - num2;
		break;
	}
	default: 
		throw "unknow operation";
	}
}

double arithmetic::StrToDouble(string str)
{
	double result = 0;
	int i;
	bool point = false;
	for (i = 0; i < str.length(); i++)
	{
		if ((str.at(i) == '.') && point) throw "wrong value";
		if (str.at(i) == '.') point = true;
	}
	if (point)
	{
		for (int p = 1, i = (str.find('.') + 1); i < str.length(); i++, p++)
		{
			if (str.at(i) <= '9' && str.at(i) >= '0')
				result += (str.at(i) - '0') * pow(10, -p);
			else
				throw "wrong value";
		}
		for (int p = 0, i = (str.find('.') - 1); i >= 0; i--, p++)
		{
			if (str.at(i) <= '9' && str.at(i) >= '0')
				result += (str.at(i) - '0') * pow(10, p);
			else
				throw "wrong value";
		}
	}
	else
		for (int p = 0, i = str.length() - 1; i >= 0; i--, p++)
		{
			if (str.at(i) <= '9' && str.at(i) >= '0')
				result += (str.at(i) - '0') * pow(10, p);
			else
				throw "wrong value";
		}
	return result;
}

bool arithmetic::IsValid(string str)
{
	bool flag = true;
	int open = 0, close = 0;
	for (int i = 0; (i < str.length()) && flag; i++)
	{
		if (str.at(i) == '(') open++;
		if (str.at(i) == ')') close++;
		if (close > open) flag = false;
	}
	if (close != open) flag = false;
	if (!flag)
	{
		TStack<int> st;
		int numOfErrors = 0, bracketNum = 0;
		cout << "Brackets" << endl;
		cout << "Opening" << " " << "Closing" << endl;
		for (int i = 0; i < str.length(); i++)
		{
			if (str.at(i) == '(')
				 st.Put(++bracketNum);
			if (str.at(i) == ')')
			{
				bracketNum++;
				if (st.IsEmpty())
				{
					numOfErrors++;
					cout << '-' << "       " << bracketNum << endl;
				}
				else
				cout << st.Get() << "       " << bracketNum << endl;
			}
		}
		while (!st.IsEmpty())
		{
			cout << st.Get() << "       " << '-' << endl;
			numOfErrors++;
		}
		cout << "Number of errors: " << numOfErrors << endl;
	}
	return flag;
}
```

###Тестовая программа для класса arithmetic

```C++
#include "arithmetic.h"

using namespace std;

int main()
{
	arithmetic exp;
	string expression;
	cout << "write expression: ";
	getline(cin,expression);
	while (expression != "exit")
	{
		try
		{
			exp.PutInfix(expression);
			cout << "result is " << exp.GetResult() << endl;
			cout << "write expression: ";
			expression.clear();
			getline(cin, expression);
		}
		catch (char *err)
		{
			cout << err << endl;
			cout << "write expression: ";
			expression.clear();
			getline(cin, expression);
		}
	}
	return 0;
}
```