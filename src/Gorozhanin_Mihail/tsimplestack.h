#ifndef _TSIMPLESTACK_
#define _TSIMPLESTACK_

#include <iostream>

template <class T>
class TSimpleStack {
private:
	int top, size;
	T* data;

public:
	TSimpleStack(int = 20);
	TSimpleStack(const TSimpleStack&);
	~TSimpleStack();
	void put(const T&);
	T get();
	void print() const;
};

template <class T>
TSimpleStack<T>::TSimpleStack(int s)
{
	if (s > 0)
	{
		size = s;
		data = new T[size];
		top = -1;
	}
	else
		throw 1;
}

template <class T>
TSimpleStack<T>::TSimpleStack(const TSimpleStack &st)
{
	size = st.size;
	top = st.top;

	data = new T[size];

	for (int i = 0; i <= top; i++)
		data[i] = st.data[i];
}

template <class T>
TSimpleStack<T>::~TSimpleStack()
{
	delete[] data;
}

template <class T>
void TSimpleStack<T>::put(const T& value)
{
	if (top < size - 1)
		data[++top] = value;
	else
		throw 2;
}

template <class T>
T TSimpleStack<T>::get()
{
	if (top > -1)
		return data[top--];
	else
		throw 3;
}

template <class T>
void TSimpleStack<T>::print() const
{
	for (int i = top; i > -1; i--)
		std::cout << data[i] << std::endl;
}
#endif
