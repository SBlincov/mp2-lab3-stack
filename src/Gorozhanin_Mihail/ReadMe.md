# Методы программирования 2: Стек
***

### Введение:


Лабораторная работа направлена на практическое освоение динамической структуры данных **Стек**. В качестве области приложений выбрана тема вычисления арифметических выражений, возникающей при трансляции программ на языке программирования высокого уровня в исполняемые программы.

При вычислении произвольных арифметических выражений возникают две основные задачи: 

- проверка корректности введённого выражения 
- выполнение операций в порядке, определяемом их приоритетами и расстановкой скобок. 

Существует алгоритм, позволяющий реализовать вычисление произвольного арифметического выражения за один просмотр без хранения промежуточных результатов. Для реализации данного алгоритма выражение должно быть представлено в постфиксной форме. 


### Цели работы:   


##### В рамках лабораторной работы ставится задача разработки двух видов стеков:  
- простейшего, основанного на статическом массиве (класс `TSimpleStack`);
- более сложного, основанного на использовании динамической структуры (класс `TStack`).
 
С помощью разработанных стеков необходимо написать приложение, которое вычисляет арифметическое выражение, заданное в виде строки и вводится пользователем. Сложность выражения ограничена только длиной строки.
***


### Задачи:  
1. Разработка класса `TSimpleStack` на основе массива фиксированной длины.
2. Реализация методов класса `TDataRoot` согласно заданному интерфейсу.
3. Разработка класса `TStack`, являющегося производным классом от `TDataRoot`.
4. Разработка тестов для проверки работоспособности стеков.
5. Реализация алгоритма проверки правильности введенного арифметического выражения.
6. Реализация алгоритмов разбора и вычисления арифметического выражения.
7. Обеспечение работоспособности тестов и примера использования.
***


### Используемые инструменты:


- Система контроля версий Git. 
- Фреймворк для написания автоматических тестов Google Test.
- Среда разработки Microsoft Visual Studio (2015).
***


# Начало работы:
***
##### Пояснения:

В процессе пользования данным классом могут возникнуть следующие исключения (ошибки): 


- Неверный размер стека. (1) 
- Стек заполнен. (2)
- Стек пуст. (3)


***
### 1. Разработка класса `TSimpleStack`:
***

#### Объявление:

```c++
#ifndef _TSIMPLESTACK_
#define _TSIMPLESTACK_

#include <iostream>

template <class T>
class TSimpleStack {
private:
	int top, size;
	T* data;

public:
	TSimpleStack(int = 20);
	TSimpleStack(const TSimpleStack&);
	~TSimpleStack();
	void put(const T&);
	T get();
	void print() const;
};

template <class T>
TSimpleStack<T>::TSimpleStack(int s)
{
	if (s > 0)
	{
		size = s;
		data = new T[size];
		top = -1;
	}
	else
		throw 1;
}

template <class T>
TSimpleStack<T>::TSimpleStack(const TSimpleStack &st)
{
	size = st.size;
	top = st.top;

	data = new T[size];

	for (int i = 0; i <= top; i++)
		data[i] = st.data[i];
}

template <class T>
TSimpleStack<T>::~TSimpleStack()
{
	delete[] data;
}

template <class T>
void TSimpleStack<T>::put(const T& value)
{
	if (top < size - 1)
		data[++top] = value;
	else
		throw 2;
}

template <class T>
T TSimpleStack<T>::get()
{
	if (top > -1)
		return data[top--];
	else
		throw 3;
}

template <class T>
void TSimpleStack<T>::print() const
{
	for (int i = top; i > -1; i--)
		std::cout << data[i] << std::endl;
}
#endif

```


#### Тесты для проверки **TSimpleStack** (GoogleTestFramework)

```c++
#include "tsimplestack.h"
#include <gtest.h>

TEST(TSimpleStack, cant_create_stack_with_negative_size)
{
	ASSERT_ANY_THROW(TSimpleStack<int>(-1));
}

TEST(TSimpleStack, can_create_stack_with_positive_length)
{
	ASSERT_NO_THROW(TSimpleStack<int>(10));
}

TEST(TSimpleStack, can_create_copied_stack)
{
	TSimpleStack<int> st1(2);

	ASSERT_NO_THROW(TSimpleStack<int> st2 = st1);
}

TEST(TSimpleStack, copied_stack_is_equal_to_source_one)
{
	TSimpleStack<int> st1(2);

	st1.put(1);
	st1.put(2);

	TSimpleStack<int> st2 = st1;

	EXPECT_EQ(1, (st2.get() == 2) && (st2.get() == 1));
}

TEST(TSimpleStack, copied_stack_has_its_own_memory)
{
	TSimpleStack<int> st1(2);

	st1.put(1);
	st1.put(2);

	TSimpleStack<int> st2 = st1;

	st1.get();

	EXPECT_EQ(2, st2.get());
}

TEST(TSimpleStack, can_put_element_when_stack_isnt_full)
{
	TSimpleStack<int> st1(2);

	ASSERT_NO_THROW(st1.put(1));
}

TEST(TSimpleStack, cant_put_element_when_stack_is_full)
{
	TSimpleStack<int> st1(2);

	st1.put(1);
	st1.put(2);

	ASSERT_ANY_THROW(st1.put(3));
}

TEST(TSimpleStack, can_get_element_when_stack_isnt_empty)
{
	TSimpleStack<int> st(2);

	st.put(2);

	ASSERT_NO_THROW(st.get());
}

TEST(TSimpleStack, cant_get_element_when_stack_is_empty)
{
	TSimpleStack<int> st(2);

	ASSERT_ANY_THROW(st.get());
}

TEST(TSimpleStack, put_and_get_correctly)
{
	TSimpleStack<int> st(1);

	st.put(1);

	EXPECT_EQ(1, st.get());
}
```


***
### 2. Реализация стека, основанного на динамичной структуре (классы `TDataCom`, `TDataRoot`, `TStack`)

TDataCom занимается обработкой кодов завершения вызовов методов и, строго говоря, особо не нужен, т.к. есть естественный способ, обработки методов (стандартный механизм исключений). 
Абстрактный класс TDataRoot, наследует TDataCom. TStack, в свою очередь, наследует TDataRoot. 
***

####Класс `TDataCom`
```c++

#ifndef __DATACOM_H__
#define __DATACOM_H__

#define DataOK   0
#define DataErr -1

class TDataCom
{
protected:
	int RetCode; 
	int SetRetCode(int ret) { return RetCode = ret; }
public:
	TDataCom() : RetCode(DataOK) {}
	virtual ~TDataCom() = 0 {}
	int GetRetCode()
	{
		int temp = RetCode;
		RetCode = DataOK;
		return temp;
	}
};

#endif
```
####Класс `TDataRoot`
```c++
#ifndef __DATAROOT_H__
#define __DATAROOT_H__

#include "tdatacom.h"

#define DefMemSize   25  

#define DataEmpty  -101  
#define DataFull   -102  
#define DataNoMem  -103  
#define SizeIncorrect -104 

enum TMemType { MEM_HOLDER, MEM_RENTER };

template <class T>
class TDataRoot : public TDataCom
{
protected:
	T* pMem;      
	int MemSize;   
	int DataCount; 
	TMemType MemType; 
	
	void SetMem(void *p, int Size);             
	
public:
	virtual ~TDataRoot();
	TDataRoot(int Size = DefMemSize);
	TDataRoot(const TDataRoot&);
	virtual bool IsEmpty(void) const;           
	virtual bool IsFull(void) const;           
	virtual void  Put(const T &Val) = 0;
	virtual T Get(void) = 0;	 
	virtual void Print() = 0;                 

											  
	friend class TMultiStack;
	friend class TSuperMultiStack;
	friend class TComplexMultiStack;
};

template <class T>
TDataRoot<T>::TDataRoot(int s) : TDataCom()
{
	MemSize = s;
	DataCount = 0;

	if (s == 0)
	{
		pMem = nullptr;
		MemType = MEM_RENTER;
	}
	else if (s > 0)
	{
		pMem = new T[MemSize];
		MemType = MEM_HOLDER;
	}
	else
		throw SetRetCode(SizeIncorrect);
}

template <class T>
TDataRoot<T>::TDataRoot(const TDataRoot &dr)
{
	MemType = MEM_HOLDER;
	MemSize = dr.MemSize;
	DataCount = dr.DataCount;

	pMem = new T[MemSize];

	for (int i = 0; i < DataCount; i++)
		pMem[i] = dr.pMem[i];
}

template <class T>
TDataRoot<T>::~TDataRoot()
{
	if (MemType == MEM_HOLDER)
		delete[] pMem;
	pMem = nullptr;
}

template <class T>
bool TDataRoot<T>::IsEmpty() const
{
	return DataCount == 0;
}

template <class T>
bool TDataRoot<T>::IsFull() const
{
	return DataCount == MemSize;
}

template <class T>
void TDataRoot<T>::SetMem(void *p, int Size)
{
	if (MemType == MEM_HOLDER)
		delete pMem;
	pMem = (T*)p;
	MemSize = Size;
	MemType = MEM_RENTER;
}

#endif
```
####Класс `TStack`
Данный стек основан на том, что в процессе работы программы выделенной при создании объекта памяти может не хватить, а значит ее нужно динамически увеличить. Количество памяти, на которое нужно увеличить имеющуюся, передается через конструктор, а по умолчанию равно 25.
```c++
#ifndef _TSTACK_
#define _TSTACK_

#include <iostream>
#include "tdataroot.h"

#define MemAddedIncorrect -105
#define DefMemAdded 25

template <class T>
class TStack : public TDataRoot<T>
{
protected:
	int top, memAdded;
	void addMem();

public:
	TStack(int Size = DefMemSize, int MemAdded = DefMemAdded);
	TStack(const TStack<T>&);
	void Put(const T&) override;
	T Get() override;
	void Print() override;
};

template <class T>
void TStack<T>::addMem()
{
	T* temp;

	temp = new T[MemSize + memAdded];

	for (int i = 0; i < DataCount; i++)
		temp[i] = pMem[i];
	delete[] pMem;
	pMem = temp;

	MemSize += memAdded;
}

template <class T>
TStack<T>::TStack(int Size, int MemAdded) : TDataRoot<T>(Size)
{
	top = -1;

	if (MemAdded > 0)
		memAdded = MemAdded;
	else
		throw SetRetCode(MemAddedIncorrect);
}

template <class T>
TStack<T>::TStack(const TStack<T> &st) : TDataRoot<T>(st)
{
	top = st.top;
	memAdded = st.memAdded;
}

template <class T>
void TStack<T>::Put(const T &data)
{
	if (pMem == nullptr)
		throw SetRetCode(DataNoMem);
	else if (IsFull())
	{
		if (MemType == MEM_RENTER)
			throw SetRetCode(DataFull);
		else
			addMem();
	}

	pMem[++top] = data;
	DataCount++;
}

template <class T>
T TStack<T>::Get()
{
	if (pMem == nullptr)
		throw SetRetCode(DataNoMem);
	else if (IsEmpty())
		throw SetRetCode(DataEmpty);
	else
	{
		DataCount--;
		return pMem[top--];
	}
}

template <class T>
void TStack<T>::Print()
{
	for (int i = top; i > -1; i--)
		std::cout << pMem[i] << std::endl;
}

#endif
```

#### Тесты для проверки **TStack** (GoogleTestFramework)

```c++
#include "tstack.h"
#include <gtest.h>

TEST(TStack, cant_create_stack_with_negative_size)
{
	ASSERT_ANY_THROW(TStack<int>(-1));
}

TEST(TStack, null_size_stack_is_created_correctly)
{
	ASSERT_NO_THROW(TStack<int>(0));
}

TEST(TStack, can_create_stack_with_positive_length)
{
	ASSERT_NO_THROW(TStack<int>(10));
}

TEST(TStack, cant_create_stack_with_negative_mem_added)
{
	ASSERT_ANY_THROW(TStack<int>(10, -1));
}

TEST(TStack, can_create_copied_stack)
{
	TStack<int> st1(2);

	ASSERT_NO_THROW(TStack<int> st2 = st1);
}

TEST(TStack, copied_stack_is_equal_to_source_one)
{
	TStack<int> st1(2);

	st1.Put(1);
	st1.Put(2);

	TStack<int> st2 = st1;

	EXPECT_EQ(1, (st2.Get() == 2) && (st2.Get() == 1));
}

TEST(TStack, copied_stack_has_its_own_memory)
{
	TStack<int> st1(2);

	st1.Put(1);
	st1.Put(2);

	TStack<int> st2 = st1;

	st1.Get();

	EXPECT_EQ(2, st2.Get());
}

TEST(TStack, can_put_element_when_stack_isnt_full)
{
	TStack<int> st1(2);

	ASSERT_NO_THROW(st1.Put(1));
}

TEST(TSimpleStack, can_put_element_when_stack_is_full)
{
	TStack<int> st(2);

	st.Put(1);
	st.Put(2);

	ASSERT_NO_THROW(st.Put(3));
}

TEST(TSimpleStack, addMem_works_correctly)
{
	TStack<int> st(2);

	st.Put(1);
	st.Put(2);
	st.Put(3);

	EXPECT_EQ(1, (st.Get() == 3) && (st.Get() == 2) && (st.Get() == 1));
}

TEST(TStack, can_get_element_when_stack_isnt_empty)
{
	TStack<int> st(2);

	st.Put(2);

	ASSERT_NO_THROW(st.Get());
}

TEST(TStack, cant_get_element_when_stack_is_empty)
{
	TStack<int> st(2);

	ASSERT_ANY_THROW(st.Get());
}

TEST(TStack, put_and_get_works_correctly)
{
	TStack<int> st(1);

	st.Put(1);

	EXPECT_EQ(1, st.Get());
}

TEST(TStack, IsEmpty_works_correctly)
{
	TStack<int> st(1);

	EXPECT_EQ(1, st.IsEmpty());
}

TEST(TStack, IsFull_works_correctly)
{
	TStack<int> st(1);

	st.Put(1);

	EXPECT_EQ(1, st.IsFull());
}
```

***
### 3. Обеспечение работоспособности тестов:
***

![](http://s13.radikal.ru/i186/1612/26/a95280732b3a.png)
***
###4. Модификация примера использования::
***
```c++
#include <iostream>
#include "tstack.h"

using namespace std;

void main()
{
	TStack<int> st(10);

	try 
	{
		for (int i = 0; i < 10; i++)
			st.Put(i);
		cout << "Our test stack is" << endl;
		for (int i = 0; i <= 10; i++) 
			cout << st.Get() << endl;
	}
	catch (int a)
	{
		cout << "Error code:" << st.GetRetCode() << endl; // вывод кода ошибки.
	}
}
```
Результат работы программы:
![](http://s011.radikal.ru/i315/1612/20/5189d4c18345.png)
***
### 5. Проверка правильности введенного арифметического выражения:
***
**5.1 Описание алгоритма**
На вход алгоритма поступает строка символов, на выходе выдается таблица соответствия номеров открывающихся и закрывающихся скобок и общее количество ошибок. Идея алгоритма, решающего поставленную задачу, состоит в следующем:

 1. Выражение просматривается посимвольно слева направо. Все символы, кроме скобок, игнорируются (т.е. просто производится переход к просмотру следующего символа).
 2. Если очередной символ – открывающая скобка, то её порядковый номер помещается в стек.
 3. Если очередной символ – закрывающая скобка, то производится выталкивание из стека номера открывающей скобки и запись этого номера в паре с номером закрывающей скобки в результирующую таблицу.
 4. Если в этой ситуации стек оказывается пустым, то вместо номера открывающей скобки записывается 0, а счетчик ошибок увеличивается на единицу.
 5. Если после просмотра всего выражения стек оказывается не пустым, то выталкиваются все оставшиеся номера открывающих скобок и записываются в результирующий массив в паре с 0 на месте номера закрывающей скобки, счетчик ошибок каждый раз увеличивается на единицу.

**5.2 Код функции проверки правильности арифметического выражения**
```c++
void IsCorrect(string expr)
{
    TStack<int> st(20, 10);
    int size = expr.length();
    int numOfErrors = 0;

    if (size <= 0)
        throw 1;

    cout << "Brackets" << endl;
    cout << "Opening" << " " << "Closing" << endl;

    for (int i = 0; i < size; i++)
    {
        if (expr[i] == '(')
            st.Put(i + 1);
        if (expr[i] == ')')
        {
            if (st.IsEmpty())
            {
                numOfErrors++;
                cout << '-' << "       " << i + 1 << endl;
            }
            else
                cout << st.Get() << "       " << i + 1 << endl;
        }
    }

    while (!st.IsEmpty())
    {
        cout << st.Get() << "       " << '-' << endl;
        numOfErrors++;
    }

    cout << "Number of errors: " << numOfErrors << endl;
}
```
Пример работы данной функции для выражения

> (a+b1)/2+6.5)*(4.8+sin(x)

![](http://s016.radikal.ru/i336/1612/95/9d9c0706fc00.png)

###6 Вычисление математических выражений
**1. Условия и ограничения**
При выполнении работы могут быть использованы следующие основные допущения:
 
 - Можно предполагать, что арифметические выражения состоят не более чем из 255 символов.
 - В качестве допустимых арифметических операций можно рассматривать только символы + (сложение), - (вычитание), * (умножение), / (деление).

**2. Алгоритм решения**
Алгоритм проверки скобок указан в пункте 5
2.1. Перевод в постфиксную форму
Данный алгоритм основан на использовании стека. На вход алгоритма поступает строка символов, на выходе должна быть получена строка с постфиксной формой. Каждой операции и скобкам приписывается приоритет.

 - ( - 0
 - ) - 1
 - +- - 2
 - */ - 3

Предполагается, что входная строка содержит синтаксически правильное выражение.
Входная строка просматривается посимвольно слева направо до достижения конца строки. Операндами будем считать любую последовательность символов входной строки, не совпадающую со знаками определённых в таблице операций. Операнды по мере их появления переписываются в выходную строку. При появлении во входной строке операции, происходит вычисление приоритета данной операции. Знак данной операции помещается в стек, если:

 1. Приоритет операции равен 0 (это « ( » ),
 2. Приоритет операции строго больше приоритета операции, лежащей на вершине стека,

В противном случае из стека извлекаются все знаки операций с приоритетом больше или равным приоритету текущей операции. Они переписываются в выходную строку, после чего знак текущей операции помещается в стек. Имеется особенность в обработке закрывающей скобки. Появление закрывающей скобки во входной строке приводит к выталкиванию и записи в выходную строку всех знаков операций до появления открывающей скобки. Открывающая скобка из стека выталкивается, но в выходную строку не записывается. Таким образом, ни открывающая, ни закрывающая скобки в выходную строку не попадают. После просмотра всей входной строки происходит последовательное извлечение всех элементов стека с одновременной записью знаков операций, извлекаемых из стека, в выходную строку.
**3. Вычисление**
Алгоритм вычисления арифметического выражения за один просмотр входной строки основан на использовании постфиксной формы записи выражения и работы со стеком
Выражение просматривается посимвольно слева направо. При обнаружении операнда производится перевод его в числовую форму и помещение в стек (если операнд не является числом, то вычисление прекращается с выдачей сообщения об ошибке.) При обнаружении знака операции происходит извлечение из стека двух значений, которые рассматриваются как операнд№2 и операнд№1 соответственно, и над ними производится обрабатываемая операция. Результат этой операции помещается в стек. По окончании просмотра всего выражения из стека извлекается окончательный результат.
**4. Реализация алгоритма вычисления значения выражения**
Код expressions.h
```c++
#ifndef _EXPRESSIONS_
#define _EXPRESSIONS_

#include <string>
using namespace std;

double CalcOfExpr(string expr); // функция, производящая непосредственное вычисление выражения по его постфиксной форме
int IsCorrect(string expr); // функция проверки на правильность расстановки скобок(Пункт 5)
string InfixToPolish(string expr); //функция, переводящая выражение из инфиксной формф в постфиксную
int GetOperationPrio(char c); // функция, возвращающая приоритет операции
#endif
```
**Код expressions.cpp**
```c++
int GetOperationPrio(char c)
{
    if (c == '(')
        return 0;
    else if (c == ')')
        return 1;
    else if (c == '+' || c == '-')
        return 2;
    else if (c == '*' || c == '/')
        return 3;
    else
        return -1;
}

double CalcOfExpr(string expr)
{
    TStack<double> st(20, 10);
    int size = expr.length(), pos = 0;
    double op1, op2;
    char ch, operandStr[20];

    if (size <= 0)
        throw 3;

    expr = InfixToPolish(expr);
    size = expr.length();

    for (int i = 0; i < size; i++)
    {
        ch = expr[i];

        if (ch == '+' || ch == '-' || ch == '*' || ch == '/' || ch == ' ')
            if (operandStr[0] != '\0')
            {
                st.Put(atof(operandStr));
                pos = 0;
                operandStr[0] = '\0';
            }

        if (ch == '+')
        {
            op1 = st.Get();
            op2 = st.Get();
            st.Put(op2 + op1);
        }
        else if (ch == '-')
        {
            op1 = st.Get();
            op2 = st.Get();
            st.Put(op2 - op1);
        }
        else if (ch == '*')
        {
            op1 = st.Get();
            op2 = st.Get();
            st.Put(op2 * op1);
        }
        else if (ch == '/')
        {
            op1 = st.Get();
            op2 = st.Get();
            st.Put(op2 / op1);
        }
        else if (ch != ' ')
        {
            operandStr[pos++] = ch;
            operandStr[pos] = '\0';
        }

    }
    return st.Get();
}

string InfixToPolish(string infixExpr)
{
    TStack<char> st(20, 10);
    bool IsOperand = false;
    int pos = 0, size = infixExpr.length();
    char ch, temp;
    string polishExpr = "";

    for (int i = 0; i < size; i++)
    {
        ch = infixExpr[i];

        if (ch == '(')
            st.Put(ch);
        else if (ch == ')')
            while (1)
            {
                temp = st.Get();
                if (temp == '(')
                    break;
                polishExpr += temp;
            }
        else if (ch == '+' || ch == '-' || ch == '*' || ch == '/')
        {
            //if (IsOperand)
            //{
                polishExpr += ' ';
                //IsOperand = false;
            //}
            if (st.IsEmpty())
                st.Put(ch);
            else
            {
                temp = st.Get();
                if (GetOperationPrio(ch) > GetOperationPrio(temp))
                {
                    st.Put(temp);
                    st.Put(ch);
                }
                else
                {
                    polishExpr += temp;
                    while (!st.IsEmpty())
                    {
                        temp = st.Get();
                        if (GetOperationPrio(temp) >= GetOperationPrio(ch))
                            polishExpr += temp;
                        else
                        {
                            st.Put(temp);
                            break;
                        }
                    }
                    st.Put(ch);
                }
            }
        }
        else
        {
            IsOperand = true;
            polishExpr += ch;
        }
    }

    while (!st.IsEmpty())
        polishExpr += st.Get();

    return polishExpr;
}
```
**5. Тестирование функций проверки правильности расстановки скобок и вычисления выражений**
Для функций IsCorrect и CalcOfExpr были написаны следующие тесты:
```c++
TEST(IsCorrect, no_errors_if_expression_is_correct)
{
    string expr = "((5+3)*7)/(34+5)*(78-45)";

    EXPECT_EQ(0, IsCorrect(expr));
}

TEST(IsCorrect, show_correct_number_of_errors)
{
    string expr1 = "((5+3*7)/)((34+5)*(78-45)";

    string expr2 = "((((";

    EXPECT_EQ(1, IsCorrect(expr1) == 1 && IsCorrect(expr2) == 4);
}

TEST(CalcOfExpr, expression_calculates_correctly)
{
    double res = (1 + 2) / (3 + 4 * 6.7) - 5.3 * 4.4;
    string expr = "(1+2)/(3+4*6.7)-5.3*4.4";

    EXPECT_EQ(res, CalcOfExpr(expr));
}
```
**Результат работы тестов для функции expression**

![](http://s019.radikal.ru/i603/1612/c7/0b5b92957de7.png)
***
# Вывод:
***
##### Все цели и задачи работы достигнуты. В ходе выполнения данной работы, мне удалось разработать два класса:
-  `TSimpleStack` - стек на основе статического массива. 
-  `TStack` - стек на основе динамической структуры. 
##### Также  мной были написаны тесты к этим классам, а также другим частям данной работы под систему тестирования `Google Test`, что позволяет не только проверить правильность работы программы, но и также лучше ознакомиться с `Google Test Framework`. Успешное же прохождение всех тестов показывает, что классы были реализованы корректно и программа созданная с использованием этих классов работает верно. 
##### Особенностью данной работы было то, что:
1. Тесты для проверки корректности работы были написаны полностью самостоятельно.
2. Была освоена структура **"Стек"**
3. Реализация данной структуры была осуществлена двумя способами: простым статичным `TSimpleStack` и более сложным в реализации и устройстве динамичным `TStack`
4. Также была рассмотрена одна из областей применения стека, такая как преобразование и вычисление арифметических выражений.
