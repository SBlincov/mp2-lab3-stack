# Методы программирования 2: Стек


## Цели и задачи

В рамках лабораторной работы была поставлена задача разработки двух видов стеков:  

- прстейшего, основанного на статическом массиве (класс `TSimpleStack`);
- более сложного, основанного на использовании динамической структуры (класс `TStack`).

С помощью разработанных стеков нужно было написать приложение, которое вычисляет значение арифметического выражения, заданного в виде строки и которое вводится пользователем. Сложность выражения ограничена только длиной строки.

Работа выполнялась на основе проекта-шаблона, содержащего следующее:

 - Интерфейсы классов `TDataCom` и `TDataRoot` (h-файлы)
 - Тестовый пример использования класса `TStack`

Результатом выполнения работы является решение следующих задач:

  1. Разработка класса `TSimpleStack` на основе массива фиксированной длины.
  1. Реализация методов класса `TDataRoot` согласно заданному интерфейсу.
  1. Разработка класса `TStack`, являющегося производным классом от `TDataRoot`.
  1. Разработка тестов для проверки работоспособности стеков.
  2. Реализация алгоритма проверки правильности введенного арифметического выражения.
  2. Реализация алгоритмов разбора и вычисления арифметического выражения.
  1. Обеспечение работоспособности тестов и примера использования.


__Реализация класса `TSimpleStack`__


```c++
#ifndef __TSIMPLESTACK_H__
#define	__TSIMPLESTACK_H__

#include <iostream>
using namespace std;

#define MemSize 25
typedef double ValType;

class TSimpleStack
{
private:
	ValType Mem[MemSize];   //память для стека
	int Top;		//индекс вершины стека
public:
	TSimpleStack()
	{
		Top = -1;
	}

	bool IsEmpty(void) const	//проверка пустоты
	{
		return Top == -1;
	}

	bool IsFull(void) const		//проверка переполнения
	{
		return Top == MemSize - 1;
	}

	void Put(const ValType val)	//добавляем значение
	{
		if (IsFull())
			throw "Stack is full";
		else
			Mem[++Top] = val;
	}
	ValType Get(void)		//извлекаем значение
	{
		if (IsEmpty())
			throw "Stack is empty";
		else
			return Mem[Top--];
	}
};

#endif
```

__Реализация класса `TDataRoot`__

```c++
#include "tdataroot.h"

TDataRoot::TDataRoot(int Size) : TDataCom()
{
	if (Size < 0)
		throw SetRetCode(DataNoMem);
	DataCount = 0;
	MemSize = Size;
	if (Size == 0) 
	{
		MemType = MEM_RENTER;
		pMem = nullptr;
	}
	else 
	{
		MemType = MEM_HOLDER;
		pMem = new TElem[MemSize];
	}
}

TDataRoot::~TDataRoot()
{
	delete[] pMem;

}

void TDataRoot::SetMem(void *p, int Size)
{
	if (Size < 0) 
		throw SetRetCode(DataNoMem);
	else if (MemType == MEM_HOLDER)
	{		
		MemSize = Size;
		PTElem temp = new TElem[MemSize];
		p = pMem;
		pMem = temp;
		temp = (PTElem)p;
		for (int i = 0; i < DataCount; i++)
			pMem[i] = temp[i];
		delete[] temp;
	}
	else
	{	
		MemSize = Size;		
		for (int i = 0; i < DataCount; i++)
			((PTElem)p)[i] = pMem[i];
		pMem = (PTElem)p;
	}
}

bool TDataRoot::IsEmpty(void) const
{
	return DataCount == 0;
}

bool TDataRoot::IsFull(void) const
{
	return DataCount == MemSize;
}
```

__Реализация класса `TStack`__

###### Интерфейс:

```c++
#ifndef __TSTACK_H__
#define __TSTACK_H__

#include "tdataroot.h"

class TStack: public TDataRoot
{
private:
	int Hi;		//индекс последнего элемента стека
public:
	TStack(int Size = DefMemSize): TDataRoot(Size), Hi(-1) {}
	virtual void  Put(const TData &val);	//добавить элемент
	virtual TData Get();			//взять элемент
	TData Top();				//значение последнего эл-та стека

	//служебные методы
	virtual int IsValid() {return 0;};      //тестирование структуры
	virtual void Print();			//печать значений стека
};

#endif
```

###### Реализация методов класса:

```c++
#include "tstack.h"
#include <iostream>
using namespace std;


void TStack::Put(const TData &val) 
{
	if (pMem == nullptr) 
		throw SetRetCode(DataNoMem);
	else if (IsFull())
		SetMem(pMem, MemSize + DefMemSize);
	pMem[++Hi] = val;
	DataCount++;
}

TData TStack::Get(void)
{
	if (pMem == nullptr)
		throw SetRetCode(DataNoMem);
	if (IsEmpty())	
		throw SetRetCode(DataEmpty);
	DataCount--;
	return pMem[Hi--];
}

TData TStack::Top()
{
	if (pMem == nullptr)
		throw SetRetCode(DataNoMem);
	if (IsEmpty())
		SetRetCode(DataEmpty);
	return pMem[Hi];
}

void TStack::Print() 
{
	if (DataCount > 0)
		for (int i = 0; i < DataCount; i++)
			cout << pMem[i] << " ";
	cout << endl;
}
```

__Тестирование__

###### Тесты для `TSimpleStack`:

```c++
#include "tsimplestack.h"

#include "gtest.h"

TEST(TSimpleStack, can_create_stack)
{
	ASSERT_NO_THROW(TSimpleStack st);
}

TEST(TSimpleStack, created_stack_is_empty)
{
	TSimpleStack st;

	EXPECT_TRUE(st.IsEmpty());
}
TEST(TSimpleStack, can_put_and_get_element)
{
	TSimpleStack st;
	int num = 5;
	st.Put(num);
	EXPECT_EQ(num, st.Get());
}

TEST(TSimpleStack, cant_get_from_empty_stack)
{
	TSimpleStack st;

	EXPECT_ANY_THROW(st.Get());
}

TEST(TSimpleStack, cant_put_in_full_stack)
{
	TSimpleStack st;
	for (int i = 0; i < MemSize; i++)
		st.Put(0);

	EXPECT_ANY_THROW(st.Put(0));
}

TEST(TSimpleStack, stack_returns_last_put_element)
{
	TSimpleStack st;
	for (int i = 0; i < MemSize; i++)
		st.Put(i);

	EXPECT_EQ(MemSize - 1, st.Get());
}

TEST(TSimpleStack, stack_is_not_empty_after_put)
{
	TSimpleStack st;
	int num = 2;
	st.Put(num);

	EXPECT_FALSE(st.IsEmpty());
}

TEST(TSimpleStack, stack_is_not_full_after_get)
{
	TSimpleStack st;
	int num = 0;
	for (int i = 0;i < MemSize;++i)
		st.Put(num);
	st.Get();

	EXPECT_FALSE(st.IsFull());
}
```

###### Для `TStack`:

```c++
#include "tstack.h"

#include <gtest.h>


TEST(TStack, can_create_stack_with_positive_length) 
{
	ASSERT_NO_THROW(TStack st(5));
}

TEST(TStack, throws_when_create_stack_with_negative_length)
{
	ASSERT_ANY_THROW(TStack st(-1));
}

TEST(TStack, created_stack_is_empty) 
{
	TStack st(4);

	EXPECT_TRUE(st.IsEmpty());
}

TEST(TStack, can_put_element)
{
	TStack st;
	st.Put(0);

	EXPECT_FALSE(st.IsEmpty());
}

TEST(TStack, can_get_element)
{
	TStack st;
	int num = 6;
	st.Put(num);

	EXPECT_EQ(num, st.Get());
}

TEST(TStack, taken_and_last_elements_are_equal)
{
	TStack st(1);
	int num = 2;
	st.Put(num);

	EXPECT_EQ(num, st.Get());
}

TEST(TStack, cant_get_from_empty_stack)
{
	TStack st;

	EXPECT_ANY_THROW(st.Get());
}

TEST(TStack, can_put_element_in_full_stack)
{
	TStack st(1);
	st.Put(1);
	st.Put(2);

	EXPECT_EQ(2, st.Get());
}

TEST(TStack, can_get_value)
{
	TStack A(2);
	A.Put(1);
	A.Put(2);
	EXPECT_EQ(2, A.Get());
}
```
Подтверждение успешного прохождения тестов, а также результат работы приложенной программы прилагается.

##### Вычисление арифметических выражений

Для решения данной задачи была разраработана библиотека `calculation`, которая содержит алгоритм `calculate` для вычисления арифметических выражений, задаваемых через строку. Сам алгоритм для удобства реализации был разделен на несколько функций.

__Реализация__

```c++
#ifndef __CALCULATION_H__
#define __CALCULATION_H__

#include <string>
#include <iostream>
#include "tstack.h"
#include "tsimplestack.h"
using namespace std;

int BracketsControl(const string str)	//проверка расстановки скобок
{
	TStack st(20);
	int size = str.length();
	int brNum = 0;
	int Errors = 0;

	cout << "Brackets control:" << endl;
	cout << "(" << "	" << ")" << endl;

	for (int i = 0; i < size; i++)
	{
		if (str[i] == '(')
			st.Put(++brNum);
		if (str[i] == ')')
		{
			brNum++;
			if (st.IsEmpty())
			{
				Errors++;
				cout << '0' << "	" << brNum << endl;
			}
			else
				cout << st.Get() << "	" << brNum << endl;
		}
	}

	while (!st.IsEmpty())
	{
		cout << st.Get() << "       " << '0' << endl;
		Errors++;
	}
	cout << "Number of errors: " << Errors << endl;

	return Errors;
}

int PriorityOf(const char ch)	//возвращает приоритет операции
{
	switch (ch)
	{
		case '(': return 0; break;
		case ')': return 1; break;
		case '+': return 2; break;
		case '-': return 2; break;
		case '*': return 3; break;
		case '/': return 3; break;
		default: return -1;
	}
}

int OperNum(const char ch)		//возвращает номер операции
{
	switch (ch)
	{
		case '(': return 10; break;
		case ')': return 11; break;
		case '+': return 1; break;
		case '-': return 2; break;
		case '*': return 3; break;
		case '/': return 4; break;
		default: return -1;
	}
}

char OperSym(const int num)		//возвращает символ операции
{
	switch (num)
	{
		case 10: return '('; break;
		case 11: return ')'; break;
		case 1: return '+'; break;
		case 2: return '-'; break;
		case 3: return '*'; break;
		case 4: return '/'; break;
		default: return -1;
	}
}

string PostfixForm(const string &instr) //преобразование к постфиксной форме
{									//(подразумевается корректность выражения)
	string poststr;
	TStack st;
	int pos = 0;
	char ch;

	while (pos < instr.size())
		{
			ch = instr[pos++];
			if (('0' <= ch) && (ch <= '9') || (ch == '.'))	//если символ - операнд
				poststr += ch;
			if (ch == '+' || ch == '-' || ch == '*' || ch == '/')			//если символ - операция
			{
				if (st.IsEmpty())
					st.Put(OperNum(ch));
				else if (PriorityOf(ch)>PriorityOf(OperSym(st.Top())))
					st.Put(OperNum(ch));
				else
				{
					while (true)
					{
						poststr += OperSym(st.Get());
						if (PriorityOf(ch) > PriorityOf(OperSym(st.Top())) || st.IsEmpty())
							break;
					}
					st.Put(OperNum(ch));
				}
				poststr += ' ';
			}
			if (ch == '(')		//если символ - открывающая скобка
				st.Put(OperNum(ch));

			if (ch == ')') // если символ - закрывающая скобка
			{			   
				while (true)
				{
					if (st.IsEmpty())
						break;
					if (st.Top() == OperNum('('))
					{
						st.Get();
						break;
					}
					poststr += OperSym(st.Get());
				}
			}
		}

		while (!st.IsEmpty()) 
			poststr += OperSym(st.Get());
		return poststr;
}



double Calculation(const string &str) //вычисление выражения в постфиксном виде
{
	
	TStack st;
	int pos = 0, i = 0, j = 0;
	char number[10] = "";
	double temp, fract;
	string postfix = PostfixForm(str);
	while (pos < postfix.size())
	{
		if (('0' <= postfix[pos]) && (postfix[pos] <= '9') || (postfix[pos] == '.')) 
			number[i++] = postfix[pos];
		else 
		{
			number[i] = '\0';
			temp = 0.0; fract = 1.0;
			for (j = 0; (number[j] != '\0') && (number[j] != '.'); j++)
				temp = temp * 10.0 + (double)(number[j] - '0');
			if (number[j] == '.')
			{
				for (j = j + 1; number[j] != '\0'; j++)
				{
					fract *= 0.1;
					temp = temp + (double)(number[j] - '0')*fract;
				}
			}
			if (j != 0)
				st.Put(temp); 
			number[0] = '\0'; 
			i = 0; 
			j = 0;
			if (postfix[pos] == ' ') 
			{
				pos++;
				continue;
			}
			temp = st.Get();
			switch (postfix[pos])
			{
				case '+': st.Put(st.Get() + temp); break;
				case '-': st.Put(st.Get() - temp); break;
				case '*': st.Put(st.Get() * temp); break;
				case '/': st.Put(st.Get() / temp); break;
			}
		}
		pos++;
	}
	return st.Get();
}


double Calculate(string &str)
{
	if (BracketsControl(str) == 0)
	{
		return Calculation(PostfixForm(str));
	}
	return 0;
}

#endif
```

__Тестирование__

```c++
#include "calculation.h"

#include <gtest.h>


TEST(Calculation, can_check_right_brackets_placement)
{
	EXPECT_EQ(0, BracketsControl("(1+2)/(3+4*6.7)-5.3*4.4"));
}

TEST(Calculation, can_detect_incorrect_brackets_placement)
{
	EXPECT_NE(0, BracketsControl("(3+1)/2+6.5)*(4.8+("));
}

TEST(Calculation, can_convert_an_expression_with_int_nums)
{
	EXPECT_EQ(PostfixForm("3*(10-5)+7"), "3 10 5-* 7+");
}

TEST(Calculation, can_convert_an_expression_with_real_nums)
{
	EXPECT_EQ(PostfixForm("(1+2)/(3+4*6.7)-5.3*4.4"), "1 2+ 3 4 6.7*+/ 5.3 4.4*-");
}

TEST(Calculation, can_calculate_an_expression)
{
	double res = (1 + 2) / (3 + 4 * 6.7) - 5.3*4.4;
	EXPECT_DOUBLE_EQ(res, Calculation("(1+2)/(3+4*6.7)-5.3*4.4"));
}

TEST(Calculation, not_calculate_an_exp_with_incorrect_placement_of_pars)
{
	ASSERT_ANY_THROW(Calculation("(1+2)(/())3+4*6.7)-5.3*4.4)("));
}

```
__Sample-приложение__

```c++
#include <iostream>
#include "tstack.h"
#include "calculation.h"
using namespace std;

void main()
{
  try
	{	
		string str = "(1+2)/(3+4*6.7)-5.3*4.4";
		cout << "Input string:" << str << endl;
		//cout << "Table brackets:" << endl;
		cout << "Answer = " << Calculation(str) << endl;
	}
	catch (int err)
	{
		cout << err << endl;
	}
}
```

Подтверждение прохождения тестов, а также результат запуска sample-приложения прилагается.

### Используемые инструменты

  - Система контроля версий [Git][git].
  - Фреймворк для написания автоматических тестов [Google Test][gtest].
  - Среда разработки Microsoft Visual Studio 2010.

### Вывод 
В ходе выполнения работы был реализован вспомогательный класс `TDataRoot`, на основе которого с нуля был написан класс `TStack`, используя такие возможности как перегрузка операций, а также наследование классов. При реализации рассматривался вариант с введением шаблонов, но в рамках данной задачи это не было критичным.

Были разработаны тесты на базе [Google Test][gtest], помогавшие с поиском ошибок в коде. В этом также помогала и sample-программа, редактирование которой помогало в более детальном поиске ошибок. Стоит отметить, что при подключении тестирующей системы к программе возникли сложности, преодоление которых помогло мне лучше понять роль различных настроек проекта.

Также было разработано приложение стека, а именно решение арифметических выражений, заданнных в строковом типе данных.

<!-- LINKS -->

[git]:         https://git-scm.com/book/ru/v2
[gtest]:       https://github.com/google/googletest