#include "tdataroot.h"

TDataRoot::TDataRoot(int Size) : TDataCom()
{
	if (Size < 0)
		throw SetRetCode(DataNoMem);
	DataCount = 0;
	MemSize = Size;
	if (Size == 0) 
	{
		MemType = MEM_RENTER;
		pMem = nullptr;
	}
	else 
	{
		MemType = MEM_HOLDER;
		pMem = new TElem[MemSize];
	}
}

TDataRoot::~TDataRoot()
{
	delete[] pMem;

}

void TDataRoot::SetMem(void *p, int Size)
{
	if (Size < 0) 
		throw SetRetCode(DataNoMem);
	else if (MemType == MEM_HOLDER)
	{		
		MemSize = Size;
		PTElem temp = new TElem[MemSize];
		p = pMem;
		pMem = temp;
		temp = (PTElem)p;
		for (int i = 0; i < DataCount; i++)
			pMem[i] = temp[i];
		delete[] temp;
	}
	else
	{	
		MemSize = Size;		
		for (int i = 0; i < DataCount; i++)
			((PTElem)p)[i] = pMem[i];
		pMem = (PTElem)p;
	}
}

bool TDataRoot::IsEmpty(void) const
{
	return DataCount == 0;
}

bool TDataRoot::IsFull(void) const
{
	return DataCount == MemSize;
}