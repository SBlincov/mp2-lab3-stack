﻿# Методы программирования 2: Стек

## Цели и задачи

В рамках лабораторной работы ставится задача разработки двух видов стеков:

- прстейшего, основанного на статическом массиве (класс `TSimpleStack`);
- более сложного, основанного на использовании динамической структуры (класс `TStack`).

С помощью разработанных стеков необходимо написать приложение, которое вычисляет арифметическое выражение, заданное в виде строки и вводится пользователем. Сложность выражения ограничена только длиной строки.

В процессе выполнения лабораторной работы требуется использовать систему контроля версий [Git][git] и фрэймворк для разработки автоматических тестов [Google Test][gtest].

Выполнение работы предполагает решение следующих задач:

  1. Разработка класса `TSimpleStack` на основе массива фиксированной длины.
  1. Реализация методов класса `TDataRoot` согласно заданному интерфейсу.
  1. Разработка класса `TStack`, являющегося производным классом от `TDataRoot`.
  1. Разработка тестов для проверки работоспособности стеков.
  2. Реализация алгоритма проверки правильности введенного арифметического выражения.
  2. Реализация алгоритмов разбора и вычисления арифметического выражения.
  1. Обеспечение работоспособности тестов и примера использования.

# Выполнение работы

####Реализация класса `TSimpleStack`
Стек `TSimpleStack` базируется на статическом массиве фиксированной длины, память для хранения данных выделяется один раз - при создании объекта. 
В конструкции класса `TSimpleStack` используются шаблоны, благодаря чему его можно применять для разных типов данных

```cpp
#ifndef __SIMPLESTACK_H__
#define __SIMPLESTACK_H__

#define MemSize   255  // размер стека по умолчанию

template<class ValType>
class TSimpleStack
{
private:
	int top;
	ValType Mem[MemSize];
public:
	TSimpleStack() { top = -1; }
	void Put(const ValType &v);
	ValType Pop();
	bool IsEmpty() const { return top == -1; }
	bool IsFull() const { return top == MemSize - 1; }
};

template <class ValType>
void TSimpleStack<ValType>::Put(const ValType &v)
{
	if (IsFull())
		throw "Стек переполнен";
	else
	{
		Mem[++top] = v;
	}
}

template <class ValType>
ValType TSimpleStack <ValType> ::Pop()
{
	if (IsEmpty())
		throw "Стек пуст";
	else
		return Mem[top--];
}

#endif
```

####Реализация класса `TDataRoot`
Класс`TDataRoot`является наследником от класса `TDataCom` и позволяет нам работать с памятью структур данных, в нём мы используем динамическую память, а также виртуальные методы.

```cpp
// ННГУ, ВМК, Курс "Методы программирования-2", С++, ООП
//
// tdataroot.h - Copyright (c) Гергель В.П. 28.07.2000 (06.08)
//   Переработано для Microsoft Visual Studio 2008 Сысоевым А.В. (21.04.2015)
//
// Динамические структуры данных - базовый (абстрактный) класс - версия 3.2
//   память выделяется динамически или задается методом SetMem

#ifndef __DATAROOT_H__
#define __DATAROOT_H__

#include "tdatacom.h"

#define DefMemSize   25  // размер памяти по умолчанию

#define DataEmpty  -101  // СД пуста
#define DataFull   -102  // СД переполнена
#define DataNoMem  -103  // нет памяти

typedef int      TElem;    // тип элемента СД
typedef TElem* PTElem;
typedef int      TData;    // тип значений в СД

enum TMemType { MEM_HOLDER, MEM_RENTER };

class TDataRoot : public TDataCom
{
protected:
	PTElem pMem;      // память для СД
	int MemSize;      // размер памяти для СД
	int DataCount;    // количество элементов в СД
	TMemType MemType; // режим управления памятью

	void SetMem(void *p, int Size);             // задание памяти
public:
	virtual ~TDataRoot();
	TDataRoot(int Size = DefMemSize);
	virtual bool IsEmpty(void) const;           // контроль пустоты СД
	virtual bool IsFull(void) const;           // контроль переполнения СД
	virtual void  Put(const TData &Val) = 0; // добавить значение
	virtual TData Get(void) = 0; // извлечь значение

	// служебные методы
	virtual int  IsValid() = 0;                 // тестирование структуры
	virtual void Print() = 0;                 // печать значений

	// дружественные классы
	friend class TMultiStack;
	friend class TSuperMultiStack;
	friend class TComplexMultiStack;
};

TDataRoot::TDataRoot(int Size) :  TDataCom()
{
	if (Size < 0)
		throw SetRetCode(DataNoMem);
	else {
		MemSize = Size;
		DataCount = 0;
		if (Size == 0) {
			MemType = MEM_RENTER;
			pMem = nullptr;
		}
		else {
			MemType = MEM_HOLDER;
			pMem = new TElem[MemSize];
		}
	}
}
TDataRoot::~TDataRoot() 
{
	if (MemType == MEM_HOLDER) 
		delete[] pMem;
	pMem = nullptr;
}
void TDataRoot::SetMem(void *p, int Size) {
	if (MemType == MEM_RENTER){
		MemSize = Size;
		for (int i = 0; i < DataCount; ++i)
			((PTElem)p)[i] = pMem[i];
		pMem = (PTElem)p;
	}
	if (MemType == MEM_HOLDER)
	{
		if (Size < 0)
			throw SetRetCode(DataNoMem);
		else {
			TElem *tmp = pMem;
			pMem = new TElem[Size];
			for (int i = 0; i < DataCount; ++i)
				pMem[i] = tmp[i];
			MemSize = Size;
			delete[]tmp;
		}
	}
}

bool TDataRoot::IsEmpty(void) const {
	return DataCount == 0;
}

bool TDataRoot::IsFull(void) const {
	return DataCount == MemSize;
}

#endif
```

####Реализация класса `TStack`
Класс`TStack`является более сложным стеком по сравнению с `TSimpleStack`, в нём мы используем динамическую память. 

```cpp
#ifndef __TDATSTACK__
#define __TDATSTACK__

#include "tdataroot.h"
#include <iostream>
using namespace std;

class TStack : public TDataRoot
{
private:
	int top;

public:
	TStack(int Size = DefMemSize) : TDataRoot(Size) { top = -1; }
	void Put(const TData & v); // добавить значение
	TData Get(); // извлечь значение
	TData GetVertex(); // получить значение на вершине стека
	int IsValid(); // тестирование структуры
	void Print(); // печать значений
};


void TStack::Put(const TData & v) { // добавить значение 
	if (!IsValid())
		SetRetCode(DataNoMem);
	else if (IsFull()) {
		void* p = nullptr;
		SetMem(p, MemSize + DefMemSize);
		pMem[++top] = v;
		DataCount++;
	}
	else {
		pMem[++top] = v;
		DataCount++;
	}
}

TData TStack::Get() { // извлечь значение
	if (!IsValid()) 
		SetRetCode(DataNoMem);
	else if (IsEmpty()) 
		SetRetCode(DataEmpty);
	else {
		DataCount--;
		return pMem[top--];
	}
	return -1;
}

TData TStack::GetVertex() { // извлечь значение
	if (!IsValid())
		SetRetCode(DataNoMem);
	else if (IsEmpty())
		SetRetCode(DataEmpty);
	else {
		return pMem[top];
	}
	return -1;
}

int TStack::IsValid() { // тестирование структуры
	if (pMem == nullptr || MemSize < DataCount || DataCount < 0 || MemSize < 0) 
		return 0;
	return 1;
}

void TStack::Print() { // печать значений
	for (int i = 0; i < DataCount; i++)
		cout << pMem[i] << " ";
	cout << endl;
}

#endif
```

#### Тестирование классов `TStack` и `TSimpleStack`

#Тесты для `TSimpleStack`
```cpp
#include "tsimplestack.h"
#include <gtest.h>

TEST(TSimpleStack, stack_is_empty)
{
	TSimpleStack<int> st;
	st.Put(1);
	st.Pop();
	EXPECT_TRUE(st.IsEmpty());
}
TEST(TSimpleStack, stack_is_full)
{
	TSimpleStack<int> st;
	for (int i = 0; i < MemSize; i++)
		st.Put(i);
	EXPECT_TRUE(st.IsFull());
}
TEST(TSimpleStack, extracted_element_is_equal_last_putted)
{
	TSimpleStack<int> st;
	st.Put(1);
	EXPECT_EQ(st.Pop(), 1);
}
TEST(TSimpleStack, cant_get_from_empty_stack)
{
	TSimpleStack<int> st;
	ASSERT_ANY_THROW(st.Pop());
}
TEST(TSimpleStack, can_pop_elem)
{
	TSimpleStack<int> st;
	st.Put(1);
	EXPECT_EQ(1, st.Pop());
}
TEST(TSimpleStack, cant_put_element_in_full_stack)
{
	TSimpleStack<int> st;
	for (int i = 0; i < MemSize; i++)
		st.Put(i);
	ASSERT_ANY_THROW(st.Put(0));
}
```
#Тесты для `TStack`
```cpp
#include "tdatstack.h"
#include <gtest.h>

TEST(TStack, can_create_stack_with_positive_length) 
{
	ASSERT_NO_THROW(TStack st(5));
}
TEST(TStack, throws_when_create_stack_with_negative_length)
{
	ASSERT_ANY_THROW(TStack st(-1));
}
TEST(TStack, stack_is_empty) 
{
	TStack st;
	st.Put(1);
	st.Get();
	EXPECT_TRUE(st.IsEmpty());
}
TEST(TStack, stack_is_full) 
{
	TStack st(1);
	st.Put(1);
	EXPECT_TRUE(st.IsFull());
}
TEST(TStack, extracted_element_is_equal_last_putted)
{
	TStack st(1);
	st.Put(1);
	EXPECT_EQ(st.Get(), 1);
}
TEST(TStack, cant_get_from_empty_stack)
{
	TStack st;
	st.Get();
	EXPECT_EQ(DataEmpty, st.GetRetCode());
}
TEST(TStack, ret_code_is_ok)
{
	TStack st;
	st.Put(1);
	EXPECT_EQ(DataOK, st.GetRetCode());
}
TEST(TStack, can_get_elem)
{
	TStack st(1);
	st.Put(1);
	EXPECT_EQ(1, st.Get());
}
TEST(TStack, can_put_element_in_full_stack)
{
	TStack st(1);
	st.Put(1);
	st.Put(2);
	EXPECT_EQ(2, st.Get());
}
TEST(TStack, can_get_ret_code_is_empty)
{
	TStack st(1);
	st.Get();
	EXPECT_EQ(DataEmpty, st.GetRetCode());
}
```

####Результаты
![](tests1.jpg)

####Реализация алгоритма проверки правильности введенного арифметического выражения и реализация алгоритмов разбора и вычисления арифметического выражения

## Алгоритм решения

### Проверка скобок


На вход алгоритма поступает строка символов, на выходе должна быть выдана таблица соответствия номеров открывающихся и закрывающихся скобок и общее количество ошибок. Идея алгоритма, решающего поставленную задачу, состоит в следующем.


- Выражение просматривается посимвольно слева направо. Все символы, кроме скобок, игнорируются (т.е. просто производится переход к просмотру следующего символа).
- Если очередной символ – открывающая скобка, то её порядковый номер помещается в стек.
- Если очередной символ – закрывающая скобка, то производится выталкивание из стека номера открывающей скобки и запись этого номера в паре с номером закрывающей скобки в результирующую таблицу.
- Если в этой ситуации стек оказывается пустым, то вместо номера открывающей скобки записывается 0, а счетчик ошибок увеличивается на единицу.
- Если после просмотра всего выражения стек оказывается не пустым, то выталкиваются все оставшиеся номера открывающих скобок и записываются в результирующий массив в паре с 0 на месте номера закрывающей скобки, счетчик ошибок каждый раз увеличивается на единицу.

## Алгоритм проверки скобок реализован в файле `culc.h` и называется `CheckBrackets`

### Перевод в постфиксную форму

Данный алгоритм основан на использовании стека.
На вход алгоритма поступает строка символов, на выходе должна быть получена строка с постфиксной формой.
Каждой операции и скобкам приписывается приоритет.

- ( - 0

- ) - 1

- +- - 2

- */ - 3


Предполагается, что входная строка содержит синтаксически правильное выражение.


Входная строка просматривается посимвольно слева направо до достижения конца строки. Операндами будем считать любую последовательность символов входной строки, не совпадающую со знаками определённых в таблице операций. Операнды по мере их появления переписываются в выходную строку. При появлении во входной строке операции, происходит вычисление приоритета данной операции. Знак данной операции помещается в стек, если:

- Приоритет операции равен 0 (это « ( » ),
- Приоритет операции строго больше приоритета операции, лежащей на вершине стека,
- Стек пуст.

В противном случае из стека извлекаются все знаки операций с приоритетом больше или равным приоритету текущей операции. Они переписываются в выходную строку, после чего знак текущей операции помещается в стек.
Имеется особенность в обработке закрывающей скобки. Появление закрывающей скобки во входной строке приводит к выталкиванию и записи в выходную строку всех знаков операций до появления открывающей скобки. Открывающая скобка из стека выталкивается, но в выходную строку не записывается. Таким образом, ни открывающая, ни закрывающая скобки в выходную строку не попадают.
После просмотра всей входной строки происходит последовательное извлечение всех элементов стека с одновременной записью знаков операций, извлекаемых из стека, в выходную строку.

## Алгоритм перевод в постфиксную форму реализован в файле `culc.h` и называется `toPostfix`

### Вычисление


Алгоритм вычисления арифметического выражения за один просмотр входной строки основан на использовании постфиксной формы записи выражения и работы со стеком

Выражение просматривается посимвольно слева направо. При обнаружении операнда производится перевод его в числовую форму и помещение в стек (если операнд не является числом, то вычисление прекращается с выдачей сообщения об ошибке.) При обнаружении знака операции происходит извлечение из стека двух значений, которые рассматриваются как операнд2 и операнд1 соответственно, и над ними производится обрабатываемая операция. Результат этой операции помещается в стек. По окончании просмотра всего выражения из стека извлекается окончательный результат.

## Алгоритм вычисления арифметического выражения реализован в файле `culc.h` и называется `CalculateFromPostfix`

# Файл  `culc.h`
```cpp
#ifndef __CALC_H__
#define __CALC_H__

#include <string>
#include <iostream>
#include "tdatstack.h"
#include "tsimplestack.h"
using namespace std;

enum SymbolsTypes { OPEN_BRACKET, CLOSE_BRACKET, SUM_OPERATIONS, MULTIPLICATION_OPERATION};


int GetSymbolPriority(char symbol) { // возвращает приоритет символа входной строки
	switch (symbol) {
	case '(':
		return OPEN_BRACKET;
		break;
	case ')':
		return CLOSE_BRACKET;
		break;
	case '+':
		return SUM_OPERATIONS;
		break;
	case '-':
		return SUM_OPERATIONS;
		break;
	case '*':
		return MULTIPLICATION_OPERATION;
		break;
	case '/':
		return MULTIPLICATION_OPERATION;
		break;
	default:
		return -1;
	}
}
int CheckBrackets(string str) { // проверка правильной расстановки скобок
	TStack brackets; // стек скобок
	int counter = 0; // счётчик скобок
	int pair_count = 0; // счётчик строк в таблице скобок
	int ** brackets_table = new int * [255]; // таблица скобок
	for (string::iterator iter = str.begin(); iter != str.end(); ++iter) {
		if (GetSymbolPriority(*iter) == OPEN_BRACKET){
			brackets.Put(pair_count);
			brackets_table[pair_count] = new int[2];
			brackets_table[pair_count][0] = ++counter;
			brackets_table[pair_count][1] = -1;
			pair_count++;
		}
		if (GetSymbolPriority(*iter) == CLOSE_BRACKET){
			if (brackets.IsEmpty()){
				brackets_table[pair_count] = new int[2];
				brackets_table[pair_count][0] = -1;
				brackets_table[pair_count][1] = ++counter;
				pair_count++;
			}
			else {
				brackets_table[brackets.Get()][1] = ++counter;
			}
		}
	}
	cout << "Brackets" << endl << "open | close" << endl;
	int errors_count = 0;
	for (int i = 0; i < pair_count; i++){
		if (brackets_table[i][0] == -1)
			errors_count++;
		if (brackets_table[i][1] == -1)
			errors_count++;
		cout << "  " << ((brackets_table[i][0] == -1) ? "-" : std::to_string(brackets_table[i][0])) << "  |  " 
			<< ((brackets_table[i][1] == -1) ? "-" : std::to_string(brackets_table[i][1])) << "  " << endl;
	}
	if (errors_count > 0)
		cout << "Errors count: " << errors_count << endl;
	else {
		cout << "No Errors" << endl;
	}
	return errors_count;
}
string toPostfix(string str){  // Перевод в постфиксную форму
	TStack operations;
	string tmp = "";
	bool operand_writing = false;
	string::iterator iter = str.begin();
	while (iter<str.end()) { // удаление пробелов из строки
		if (*iter == ' ') {
			str.erase(iter);
		}
		else
			iter++;
	}
	for (string::iterator iter = str.begin(); iter != str.end(); ++iter) {
		int priority = GetSymbolPriority(*iter);
		if (priority == -1){
			tmp += *iter;
			if (operand_writing == false)
				operand_writing = true;
		}
		else {
			if (operand_writing){
				operand_writing = false;
				tmp += " ";
			}
			if (priority == 1) {
				while (GetSymbolPriority(operations.GetVertex()) != 0) 
					tmp += operations.Get();
			}
			else if (priority == 0 || priority > GetSymbolPriority(operations.GetVertex()) || operations.IsEmpty())
				operations.Put(*iter);
			else {
				while (GetSymbolPriority(operations.GetVertex()) >= priority)
					tmp += operations.Get();
				tmp += " ";
				operations.Put(*iter);
			}
		}
	}
	while (!operations.IsEmpty()) {
		if (GetSymbolPriority(operations.GetVertex()) > 1)
			tmp += operations.Get();
		else
			operations.Get();
	}
	return tmp;
}

int isValueNumber(string s_param)
{
	for (int i = 0; i < s_param.size(); i++)
	{
		if ((s_param[i] >= '0') && (s_param[i] <= '9') || s_param[i] == '.')
			continue;
		else
			return 0;
	}
	return 1;
}

double CalculateFromPostfix(string str){
	TSimpleStack<double> operands;
	string operand;
	for (string::iterator iter = str.begin(); iter<str.end(); ++iter) {
		int priority = GetSymbolPriority(*iter);
		if (priority == -1 && *iter != ' ') {
			operand += *iter;
		}
		else {
			if (operand.length() > 0) {
				if (isValueNumber(operand))
					operands.Put(atof(operand.c_str()));
				else {
					cout << "Operand is not a number" << endl;
					return 0;
				}
				operand.clear();
			}
			if (priority >= 2) { 
				double val[2], result = 0;
				for (int i = 0; i < 2; i++) {
					val[i] = operands.Pop();
				}
				switch (*iter) {
				case '+':
					result = val[0] + val[1];
					break;
				case '-':
					result = val[1] - val[0];
					break;
				case '*':
					result = val[0] * val[1];
					break;
				case '/':
					result = val[1] / val[0];
					break;
				}
				operands.Put(result);
			}
		}
	}
	return operands.Pop();
}

double Calculate(string str){
	if (CheckBrackets(str) == 0)
		return CalculateFromPostfix(toPostfix(str));
	else {
		cout << "Error expression" << endl;
	}
	return 0;
}
#endif
```

# Тестирование функций из `culc.h`
```cpp
#include "calc.h"
#include <gtest.h>

TEST(calc, check_brackets)
{
	std::string st = "(1+2*(3+4))";
	EXPECT_EQ(CheckBrackets(st),0);
}
TEST(calc, convert_to_postfix)
{
	std::string st = "(1+2*(3+4))";
	EXPECT_EQ(toPostfix(st), "1 2 3 4 +*+");
}
TEST(calc, is_value_number)
{
	EXPECT_TRUE(isValueNumber("4.54"));
}
TEST(calc, calculate_from_postfix)
{
	EXPECT_EQ(CalculateFromPostfix("1 2 3 4 +*+"),15);
}
TEST(calc, calculate)
{
	EXPECT_EQ(Calculate("(1+2*(3+4))"), 15);
}
```

####Результаты
![](tests2.jpg)


#### Обеспечение работоспособности тестов и примера использования

# Файл `main.cpp`
```cpp
// ННГУ, ВМК, Курс "Методы программирования-2", С++, ООП
//
// Copyright (c) Гергель В.П. 28.07.2000
//   Переработано для Microsoft Visual Studio 2008 Сысоевым А.В. (21.04.2015)
//
// Динамические структуры данных - тестирование стека

#include <iostream>
#include "tdatstack.h"

using namespace std;

void main()
{
	TStack st(2);
	int temp;
	setlocale(LC_ALL, "Russian");
	cout << "Тестирование программ поддержки структуры типа стека" << endl;
	for (int i = 0; i < 35; i++)
	{
		st.Put(i);
		cout << "Положили значение " << i << " Код " << st.GetRetCode() << endl;
	}
	while (!st.IsEmpty())
	{
		temp = st.Get();
		cout << "Взяли значение " << temp << " Код " << st.GetRetCode() << endl;
	}
}
```
#Скриншоты 
![](sample.jpg)

####Вывод

В результате проделанной работы были реализованы два вида "Стеков": класс `TSimpleStack`, основанный на статическом массиве и класс `TStack` - на динамическом. Нами применялся такой подход в ООП как наследование, а также использовались шаблоны, что позволило сократить время разработи, упростило её и облегчило реализацию стеков.
Были реализованы алгоритмы проверки правильности введенного арифметического выражения, его разбора и вычисления.

Для тестирования работоспособности разработанных классов и функций, а также выявления ошибок использовалась система Google Test, были успешно пройдены все тесты. 
[git]:         https://git-scm.com/book/ru/v2
[gtest]:       https://github.com/google/googletest
[sieve]:       http://habrahabr.ru/post/91112
[travis]:      https://travis-ci.org/UNN-VMK-Software/mp2-lab1-set
[git-guide]:   https://bitbucket.org/ashtan/mp2-lab1-set/src/ff6d76c3dcc2a531cefdc17aad5484c9bb8b47c5/docs/part1-git.md?at=master&fileviewer=file-view-default
[gtest-guide]: https://bitbucket.org/ashtan/mp2-lab1-set/src/ff6d76c3dcc2a531cefdc17aad5484c9bb8b47c5/docs/part2-google-test.md?at=master&fileviewer=file-view-default
[youtube-playlist]: https://www.youtube.com/playlist?list=PLSzOhsr5tmhrgV7u7CSzX4Ki1a9r0AKzV
[slides]:      https://bitbucket.org/ashtan/mp2-lab1-set/src/ff6d76c3dcc2a531cefdc17aad5484c9bb8b47c5/docs/slides/?at=master
[upstream]:    https://bitbucket.org/ashtan/mp2-lab1-set
