// ����, ���, ���� "������ ����������������-2", �++, ���
//
// tdataroot.h - Copyright (c) ������� �.�. 28.07.2000 (06.08)
//   ������������ ��� Microsoft Visual Studio 2008 �������� �.�. (21.04.2015)
//
// ������������ ��������� ������ - ������� (�����������) ����� - ������ 3.2
//   ������ ���������� ����������� ��� �������� ������� SetMem

#ifndef __DATAROOT_H__
#define __DATAROOT_H__

#include "tdatacom.h"

#define DefMemSize   25  // ������ ������ �� ���������

#define DataEmpty  -101  // �� �����
#define DataFull   -102  // �� �����������
#define DataNoMem  -103  // ��� ������

typedef int      TElem;    // ��� �������� ��
typedef TElem* PTElem;
typedef int      TData;    // ��� �������� � ��

enum TMemType { MEM_HOLDER, MEM_RENTER };

class TDataRoot : public TDataCom
{
protected:
	PTElem pMem;      // ������ ��� ��
	int MemSize;      // ������ ������ ��� ��
	int DataCount;    // ���������� ��������� � ��
	TMemType MemType; // ����� ���������� �������

	void SetMem(void *p, int Size);             // ������� ������
public:
	virtual ~TDataRoot();
	TDataRoot(int Size = DefMemSize);
	virtual bool IsEmpty(void) const;           // �������� ������� ��
	virtual bool IsFull(void) const;           // �������� ������������ ��
	virtual void  Put(const TData &Val) = 0; // �������� ��������
	virtual TData Get(void) = 0; // ������� ��������

	// ��������� ������
	virtual int  IsValid() = 0;                 // ������������ ���������
	virtual void Print() = 0;                 // ������ ��������

	// ������������� ������
	friend class TMultiStack;
	friend class TSuperMultiStack;
	friend class TComplexMultiStack;
};

TDataRoot::TDataRoot(int Size) :  TDataCom()
{
	if (Size < 0)
		throw SetRetCode(DataNoMem);
	else {
		MemSize = Size;
		DataCount = 0;
		if (Size == 0) {
			MemType = MEM_RENTER;
			pMem = nullptr;
		}
		else {
			MemType = MEM_HOLDER;
			pMem = new TElem[MemSize];
		}
	}
}
TDataRoot::~TDataRoot() 
{
	if (MemType == MEM_HOLDER) 
		delete[] pMem;
	pMem = nullptr;
}
void TDataRoot::SetMem(void *p, int Size) {
	if (MemType == MEM_RENTER){
		MemSize = Size;
		for (int i = 0; i < DataCount; ++i)
			((PTElem)p)[i] = pMem[i];
		pMem = (PTElem)p;
	}
	if (MemType == MEM_HOLDER)
	{
		if (Size < 0)
			throw SetRetCode(DataNoMem);
		else {
			TElem *tmp = pMem;
			pMem = new TElem[Size];
			for (int i = 0; i < DataCount; ++i)
				pMem[i] = tmp[i];
			MemSize = Size;
			delete[]tmp;
		}
	}
}

bool TDataRoot::IsEmpty(void) const {
	return DataCount == 0;
}

bool TDataRoot::IsFull(void) const {
	return DataCount == MemSize;
}

#endif