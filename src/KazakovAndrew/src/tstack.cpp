#include <iostream>
#include "tstack.h"

using namespace std;

void TStack::Put(const TData& Val) {

	if (pMem == nullptr) SetRetCode(DataNoMem);
	else if (IsFull()) SetRetCode(DataFull);
	else {
		pMem[++top] = Val;
		DataCount++;
	}

}

TData TStack::Get() {

	if (pMem == nullptr) SetRetCode(DataNoMem);
	else if (IsEmpty()) SetRetCode(DataEmpty);
	else {
		DataCount--;
		return pMem[top--];
	}
	return -1;

}

void TStack::Print() {

	for (int i = 0; i < DataCount; i++)
		cout << pMem[i] << " ";
	cout << endl;

}