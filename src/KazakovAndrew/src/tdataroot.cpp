#include "tdataroot.h"

TDataRoot::TDataRoot(int Size): TDataCom() {

	DataCount = 0;
	MemSize = Size;

	if (Size == 0) {
		MemType = MEM_RENTER;
	} else {
		MemType = MEM_HOLDER;
		pMem = new TElem[MemSize];
	}

}

TDataRoot::~TDataRoot() {

	delete[] pMem;

}

void TDataRoot::SetMem(void* p, int Size) {

	if (MemType == MEM_HOLDER) {
		delete[] pMem;
	}

	MemType = MEM_RENTER;
	pMem = (PTElem)p;
	MemSize = Size;

}

bool TDataRoot::IsEmpty(void) const {

	return DataCount == 0;

}

bool TDataRoot::IsFull(void) const {

	return DataCount == MemSize;

}