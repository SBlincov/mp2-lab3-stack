# Методы программирования 2: Стек


## Цели и задачи

В рамках лабораторной работы была поставлена задача разработки двух видов стеков:  

- прстейшего, основанного на статическом массиве (класс `TSimpleStack`);
- более сложного, основанного на использовании динамической структуры (класс `TStack`).

С помощью разработанных стеков нужно было написать приложение, которое вычисляет значение арифметического выражения, заданного в виде строки и которое вводится пользователем. Сложность выражения ограничена только длиной строки.

Работа выполнялась на основе проекта-шаблона, содержащего следующее:

 - Интерфейсы классов `TDataCom` и `TDataRoot` (h-файлы)
 - Тестовый пример использования класса `TStack`

Результатом выполнения работы является решение следующих задач:

  1. Разработка класса `TSimpleStack` на основе массива фиксированной длины.
  1. Реализация методов класса `TDataRoot` согласно заданному интерфейсу.
  1. Разработка класса `TStack`, являющегося производным классом от `TDataRoot`.
  1. Разработка тестов для проверки работоспособности стеков.
  2. Реализация алгоритма проверки правильности введенного арифметического выражения.
  2. Реализация алгоритмов разбора и вычисления арифметического выражения.
  1. Обеспечение работоспособности тестов и примера использования.

 __Реализация класса `TSimpleStack`:__

```c++
#ifndef _TSTACK_H_
#define _TSTACK_H_

#define MemSize 25 // размер памяти для стека

class TSimpleStack {
	protected:
		int Mem[MemSize]; // память для СД
		int Top; // индекс последнего занятого элемента в Mem - вершина стека
	public:
		TSimpleStack() { Top = -1; }
		bool IsEmpty() const { return Top == -1; } // контроль пустоты
		bool IsFull() const { return Top == MemSize - 1; } // контроль переполнения
		void Push(const int Val) // добавить значение
		{ 
			if (IsFull())
				throw("Stack is full");
			else
				Mem[++Top] = Val;
		} 
		int Pop() // извлечь значение
		{ 
			if (IsEmpty())
				throw("Stack is empty");
			else
				return Mem[Top--];
		}
};
#endif
```

__Реализация методов класса `TDataRoot`:__

```c++
// Динамические структуры данных - базовый (абстрактный) класс 
//   память выделяется динамически или задается методом SetMem

#include <stdio.h>
#include <tdataroot.h>

	/*-------------------------------------------*/
	
TDataRoot :: TDataRoot (int Size): TDataCom(), MemSize(Size), DataCount(0){
	if(Size == 0) // память будет выделена методом SetMem
	{
		pMem = NULL; 
		MemType = MEM_RENTER;
	}
	else // память будет выделятся самим объектом
	{	
		pMem = new TElem[MemSize];
		MemType = MEM_HOLDER;
	}	
}	

	/*-------------------------------------------*/
	
TDataRoot :: ~TDataRoot()
{
	if(MemType == MEM_HOLDER)
		delete[] pMem;
	pMem = NULL;
}	
	
	/*-------------------------------------------*/

void TDataRoot :: SetMem(void *p, int Size) // задание памяти
{
	if(MemType == MEM_HOLDER)
		delete pMem; // информация не сохраняется
	MemType = MEM_RENTER;
	pMem = (TElem *)p;
	MemSize = Size;
}

	/*-------------------------------------------*/

bool TDataRoot :: IsEmpty() const  // контроль пустоты СД
{
	return DataCount == 0;
} 	

	/*-------------------------------------------*/

bool TDataRoot :: IsFull() const // контроль переполнения СД
{
	return DataCount == MemSize;
}
```

__Реализация класса `TStack`:__

Интерфейс:

```c++
// Динамические структуры данных - стек

#ifndef _DATSTACK_H_
#define _DATSTACK_H_

#include "tdataroot.h"

class TStack: public TDataRoot
{
	protected:
		int Hi; // индекс последнего элемента структуры
		virtual int GetNextIndex(int Index);
	public: 
		TStack(int Size = DefMemSize): TDataRoot(Size)
		{
			Hi = -1;
		}
		virtual void Push(const TData &Val); // положить в стек
		virtual TData Pop(void); //взять из стека
		virtual TData TopElem(void); // посмотреть значение вершины стека
	protected:
		// служебные методы
		virtual int IsValid(void);	// тестирование структуры
		virtual void Print(void);	// печать значений
};
#endif
```

Методы класса:

```c++
// Динамические структуры данных - стек

#include <stdio.h>
#include "tdatstack.h"

void TStack :: Push(const TData &Val) // положить элемент в стек
{
	if (pMem == NULL)
		SetRetCode(DataNoMem);
	else
		if(IsFull())
			SetRetCode(DataFull);
	else
	{
		Hi = GetNextIndex(Hi);
		pMem[Hi] = Val;
		DataCount++;
	}	
}

	/*-------------------------------------------*/

TData TStack :: Pop() // взять элемент из стека
{
	TData temp = -1;
	if(pMem == NULL)
		SetRetCode(DataNoMem);
	else
		if(IsEmpty())
			SetRetCode(DataEmpty);
	else
	{
		temp = pMem[Hi--];
		DataCount--;
	}
	return temp;
}

	/*-------------------------------------------*/

TData TStack::TopElem() // посмотреть значение вершины стека
{
	if (pMem == NULL)
		SetRetCode(DataNoMem);
	else
		if (IsEmpty())
			SetRetCode(DataEmpty);
		else
			return pMem[Hi];
}

	/*-------------------------------------------*/

int TStack :: GetNextIndex(int index) {return ++index;} // получить следующее значение индекса

int  TStack :: IsValid()// тестирование структуры
{
	int res = 0;
	if (pMem == nullptr)
		res++;
	if (MemSize < DataCount)
		res += 2;
	return res;
}
	/*-------------------------------------------*/

void TStack :: Print() // печать значений стека
{
	for(int i = 0; i < DataCount; i++)
		printf("%d", pMem[i]);
	printf("\n");
}	
```

__Тесты, разработанные для проверки работоспособности стеков:__

Для `TSimpleStack`:

```c++
#include <gtest/gtest.h>

#include <tsimplestack.h>

TEST(TSimpleStack, can_create_stack)
{
	ASSERT_NO_THROW(TSimpleStack s1());
}

TEST(TSimpleStack, new_stack_is_empty)
{
	TSimpleStack s1;
	EXPECT_EQ(true, s1.IsEmpty());
}

TEST(TSimpleStack, can_push_elem_in_stack)
{
	TSimpleStack s1;
	int elem = 1;
	ASSERT_NO_THROW(s1.Push(elem));
}

TEST(TSimpleStack, stack_with_elem_isnt_empty)
{
	TSimpleStack s1;
	int elem = 1;
	s1.Push(elem);
	EXPECT_EQ(false, s1.IsEmpty());
}

TEST(TSimpleStack, cant_push_in_full_stack)
{
	TSimpleStack s1;
	int i;
	for (i = 0; i < MemSize; i++)
		s1.Push(i);
	ASSERT_ANY_THROW(s1.Push(i + 1));
}


TEST(TSimpleStack, can_pop_elem_from_stack)
{
	TSimpleStack s1;
	int elem1 = 1, elem2;
	s1.Push(elem1);
	elem2 = s1.Pop();
	EXPECT_EQ(elem1, elem2);
}

TEST(TSimpleStack, cant_pop_from_empty_stack)
{
	TSimpleStack s1;
	ASSERT_ANY_THROW(s1.Pop());
}

TEST(TSimpleStack, pop_returns_last_pushed_elem)
{
	TSimpleStack s1;
	int elem1 = 1, elem2 = 2, res;
	s1.Push(elem1); s1.Push(elem2);
	res = s1.Pop();
	EXPECT_TRUE((res = elem2) && (res != elem1));
}

TEST(TSimpleStack, full_stack_after_pop_isnt_full)
{
	TSimpleStack s1;
	int i;
	for (i = 0; i < MemSize; i++)
		s1.Push(i);
	bool isf = s1.IsFull();
	int val = s1.Pop();
	EXPECT_TRUE(isf != s1.IsFull());
}
```

Для `TStack`:

```c++
#ifdef USE_TESTS

#include <gtest/gtest.h>

#include <tdatstack.h>

TEST(TStack, can_create_stack)
{
	ASSERT_NO_THROW(TStack s1);
}

TEST(TStack, can_create_stack_with_set_size)
{
	ASSERT_NO_THROW(TStack s1(5));
}

TEST(TStack, new_stack_is_empty)
{
	TStack s1;
	EXPECT_EQ(true, s1.IsEmpty());
}

TEST(TStack, can_push_elem_in_stack)
{
	TStack s1;
	int elem = 1;
	ASSERT_NO_THROW(s1.Push(elem));
}

TEST(TStack, stack_with_elem_isnt_empty)
{
	TStack s1;
	int elem = 1;
	s1.Push(elem);
	EXPECT_EQ(false, s1.IsEmpty());
}

TEST(TStack, cant_push_in_full_stack)
{
	TStack s1;
	int i;
	for (i = 0; i < DefMemSize; i++)
		s1.Push(i);
	s1.Push(i + 1);
	EXPECT_EQ(DataFull, s1.GetRetCode());
}


TEST(TStack, can_pop_elem_from_stack)
{
	TStack s1;
	int elem1 = 1, elem2;
	s1.Push(elem1);
	elem2 = s1.Pop();
	EXPECT_EQ(elem1, elem2);
}

TEST(TStack, cant_pop_from_empty_stack)
{
	TStack s1;
	s1.Pop();
	EXPECT_EQ(DataEmpty, s1.GetRetCode());
}

TEST(TStack, pop_returns_last_pushed_elem)
{
	TStack s1;
	int elem1 = 1, elem2 = 2, res;
	s1.Push(elem1); s1.Push(elem2);
	res = s1.Pop();
	EXPECT_TRUE((res = elem2) && (res != elem1));
}

TEST(TStack, full_stack_after_pop_isnt_full)
{
	TStack s1;
	int i;
	for (i = 0; i < DefMemSize; i++)
		s1.Push(i);
	bool isf = s1.IsFull();
	int val = s1.Pop();
	EXPECT_TRUE(isf != s1.IsFull());
}
```

__Подтверждение успешного прохождения тестов для стеков:__

![GTests_4_Stack.jpg](http://i.imgur.com/NTPFh0Q.jpg "GTests_4_Stack.jpg")


__Результат работы предложенной программы, которая использует класс `TStack`:__

Размер стека был установлен меньше, чем число элементов, для того, чтобы отследить работу кодов завершения.

![Sampl1.jpg](http://i.imgur.com/QE3yyB6.jpg "Sample1.jpg")

##Вычисление арифметических выражений

Для решения данной задачи была разраработана библиотека `expressions`, содержащая алгоритмы для вычисления арифметических выражений, задаваемых строкой.

 __Функции, доступные пользователю, описаны в `expressions.h`:__  

```c++
#ifndef EXP_H
#define EXPR_H

#include <iostream>
#include <string>
#include "tdatstack.h"
#include "tsmplstack_double.h"

using namespace std;

#define ExpSize 255

bool BrControl(const string &, bool); // проверка коррекности расстановки круглых скобок
string ConvertToPostfix (const string &); // преобразвание к постфиксной записи
double Calculation(const string &);	// вычисление значения выражения, записанного в префиксном виде

#endif
```

 __Реализация алгоритмов `expressions.h` и вспомогательных функций находится в `expressions.cpp`:__  

```c++
#include "expressions.h"

int GetOpPriority(char op) // получить приоритет операции
{
	switch (op)
	{
		case '(': return 0;
		case ')': return 1;
		case '+': return 2;
		case '-': return 2;
		case '*': return 3;
		case '/': return 3;
		default: return -1;
	}
}

	/*-------------------------------------------*/

bool IsOperation(char op) // проверка на принадлежность к знакам операций
{
	if (op == '+' || op == '-' || op == '*' || op == '/')
		return true;
	return false;
}

	/*-------------------------------------------*/

bool BrControl(const string &exp, bool print) // проверка коррекности расстановки круглых скобок
{
	TStack CtrlTable(ExpSize);
	int pos = 0, errcount = 0, index = 0, temp;
	setlocale(LC_ALL, "Russian");
	while (pos < exp.size())
	{
		if (exp[pos] == '(')
			CtrlTable.Push(++index);
		if (exp[pos] == ')')
			if (CtrlTable.IsEmpty()) // не достаёт '('
			{
				errcount++;
				index++;
				if (print) cout << '-' << ' ' << index << endl;
		}
			else
			{
				index++;
				temp = (int)CtrlTable.Pop();
				if (print) cout << temp << ' ' << index << endl;
			}
		pos++;
	}

	// если стек не пуст, то значит не достаёт закрывающихся скобок
	if (!CtrlTable.IsEmpty())
	{
		errcount++;
		temp = (int)CtrlTable.Pop();
		if (print) cout << temp << ' ' << '-' << endl;
	}

	if (errcount == 0)
	{
		if (print) cout << "Ошибок в расстановке скобок нет" << endl;
		return true;
	}
	else
	{
		if (print) cout << "В расстановке скобок есть ошибки." << "Выражение не может быть вычислено" << endl;
		return false;
	}
}

	/*-------------------------------------------*/

// преобразование выражения от инфиксной к постфиксной форме
// считается, что выражние правильное, без пробелов

string ConvertToPostfix(const string &InfixExp) // преобразвание к постфиксной записи
{
	try{
		if (!BrControl(InfixExp, false))
			throw 1;
		char ch;
		string	PrefixExp;
		int pos = 0; // индекс текущего символа в выражении
		TStack OperationStack(ExpSize);
		while (pos < InfixExp.size())
		{
			ch = InfixExp[pos++];
			if (('0' <= ch) && (ch <= '9') || (ch == '.')) // операнд
				PrefixExp += ch;
			if (IsOperation(ch))
			{
				if (OperationStack.IsEmpty()) // если стек пустой, помещаем операцию в конец стека
					OperationStack.Push(ch);
				else
				if (GetOpPriority(ch)>GetOpPriority(OperationStack.TopElem()))
					OperationStack.Push(ch);
				else
				{
					while (true)
					{
						PrefixExp += OperationStack.Pop();
						if (GetOpPriority(ch) > GetOpPriority(OperationStack.TopElem()) || OperationStack.IsEmpty())
							break;
					}
					// Добавляем в стек текущую операцию
					OperationStack.Push(ch);
				}
				//если не число, добавляем пробел, чтобы в дальнейшем можно было отличить числа
				PrefixExp += ' ';
			}
			if (ch == '(')
				OperationStack.Push(ch);

			if (ch == ')') // в этом случае извлекаем из стека операций в результирующую строку все операции,
			{			   // пока не встретим открывающую скобку. Сами скобки при этом уничтожаются
				while (true)
				{
					if (OperationStack.IsEmpty())
						break;
					if (OperationStack.TopElem() == '(')
					{
						OperationStack.Pop();
						break;
					}
					PrefixExp += OperationStack.Pop();
				}
			}
		}
		while (!OperationStack.IsEmpty()) // добавляем оставшиеся в стеке операции, если они остались
			PrefixExp += (OperationStack.Pop());
		return PrefixExp;
	}
	catch (int i)
	{
		throw("The expression cannot be calculated");
	}
}

	/*-------------------------------------------*/

double Calculation(const string &exp) // вычисление значения выражения
{
	int i = 0, j = 0, k = 0;
	TSimpleStackD calc;
	char number[ExpSize] = "";
	double temp, fract;
	string PostFix = ConvertToPostfix(exp);
	while (i < PostFix.size())
	{
		if (('0' <= PostFix[i]) && (PostFix[i] <= '9') || (PostFix[i] == '.')) //собираем число
			number[j++] = PostFix[i];
		else // число кончилось
		{
			number[j] = '\0';
			temp = 0.0; fract = 1.0;
			for (k = 0; (number[k] != '\0') && (number[k] != '.'); k++)
				temp = temp * 10.0 + (double)(number[k] - '0');
			if (number[k] == '.')
			{
				for (k = k + 1; number[k] != '\0'; k++)
				{
					fract *= 0.1;
					temp = temp + (double)(number[k] - '0')*fract;
				}
			}
				if (k != 0) // заносим в стек только те значения, которые вычисляли
					calc.Push(temp); 
				number[0] = '\0'; j = 0; k = 0;
			if (PostFix[i] == ' ') // пробел - основной признак конца числа, переходим к следующей итерации цикла
			{
				i++;
				continue;
			}
			// берем 2 элемента из стека, вычисляем результат операции
			temp = calc.Pop();
			switch (PostFix[i])
			{
				case '+': calc.Push(calc.Pop() + temp); break;
				case '-': calc.Push(calc.Pop() - temp); break;
				case '*': calc.Push(calc.Pop() * temp); break;
				case '/': calc.Push(calc.Pop() / temp); break;
			}
		}
		i++;
	}
	// в итоге последним элементом в стеке будет являться результ
	return calc.Pop();
}
```

__Тесты для проверки корректности вычисления арифметических выражений с помощью библиотеки `expressions`__:

```c++
#include <gtest/gtest.h>
#include "expressions.h"

TEST(BrControl, can_check_parentheses_placement)
{
	EXPECT_EQ(true, BrControl("(1+2)/(3+4*6.7)-5.3*4.4", false));
}

TEST(BrControl, can_detect_incorrect_placement_of_parentheses)
{
	EXPECT_EQ(false, BrControl("(a+b1)/2+6.5)*(4.8+(", false));
}

TEST(ConvertToPostfix, can_convert_an_expression_with_int_nums)
{
	EXPECT_EQ(ConvertToPostfix("3*(10-5)+7"), "3 10 5-* 7+");
}

TEST(ConvertToPostfix, can_convert_an_expression_with_real_nums)
{
	EXPECT_EQ(ConvertToPostfix("(1+2)/(3+4*6.7)-5.3*4.4"), "1 2+ 3 4 6.7*+/ 5.3 4.4*-");
}


TEST(ConvertToPostfix, can_convert_an_expression_with_many_digit_numbers)
{
	EXPECT_EQ(ConvertToPostfix("100.00000+20000/3500.01*(1000+21000000.123)"), "100.00000 20000 3500.01/ 1000 21000000.123+*+");
}

TEST(ConvertToPostfix, not_covnert_an_exp_with_incrrt_placement_of_pars)
{
	ASSERT_ANY_THROW(ConvertToPostfix("2*(8-5)+4)"));
}

TEST(Calculation, can_calculate_a_simple_expression)
{
	double res = (1 + 2) / (3 + 4 * 6.7) - 5.3*4.4;
	EXPECT_DOUBLE_EQ(Calculation("(1+2)/(3+4*6.7)-5.3*4.4"), res);
}

TEST(Calculation, can_calculate_an_expression_with_many_digit_numbers)
{
	double res = 100.0 + 20000.120 / 2500.0*(1000.0 + 20100000.0008);
	EXPECT_DOUBLE_EQ(Calculation("100.0+20000.120/2500.0*(1000.0+20100000.0008)"), res);
}

TEST(Calculation, not_calculate_an_exp_with_incorrect_placement_of_pars)
{
	ASSERT_ANY_THROW(Calculation("(1+2)(/())3+4*6.7)-5.3*4.4)("));
}
```
__Подтверждение успешного прохождения тестов для проверки алгоритмов:__

![Exp_Tests.jpg](http://i.imgur.com/jCpbnqP.jpg "Exp_Tests.jpg")


## Используемые инструменты

  - Система контроля версий [Git][git].
  - Фреймворк для написания автоматических тестов [Google Test][gtest].
  - Среда разработки Microsoft Visual Studio 2013.

##Вывод  
Основные цели работы достигнуты. Стоит отметить, что для успешного применения классов стеков в решении прикладных задач может потребоваться введение шаблонов в их реализацию. Это обеспечит комфортную работу с различными типам данных. Однако для поставленной задачи было достаточно указать типы данных заранее. 

В ходе выполнения данной работы возникли некоторые трудности при создании решения и работе с [Google Test][gtest]. Решение этих проблем помогло лучше понять предназначение различных настроек проекта.

<!-- LINKS -->

[git]:         https://git-scm.com/book/ru/v2
[gtest]:       https://github.com/google/googletest
[sieve]:       http://habrahabr.ru/post/91112
[git-guide]:   https://bitbucket.org/ashtan/mp2-lab1-set/src/ff6d76c3dcc2a531cefdc17aad5484c9bb8b47c5/docs/part1-git.md?at=master&fileviewer=file-view-default
[gtest-guide]: https://bitbucket.org/ashtan/mp2-lab1-set/src/ff6d76c3dcc2a531cefdc17aad5484c9bb8b47c5/docs/part2-google-test.md?at=master&fileviewer=file-view-default
[upstream]:    https://bitbucket.org/ashtan/mp2-lab1-set

