#include "expressions.h"

int GetOpPriority(char op) // �������� ��������� ��������
{
	switch (op)
	{
		case '(': return 0;
		case ')': return 1;
		case '+': return 2;
		case '-': return 2;
		case '*': return 3;
		case '/': return 3;
		default: return -1;
	}
}

	/*-------------------------------------------*/

bool IsOperation(char op) // �������� �� �������������� � ������ ��������
{
	if (op == '+' || op == '-' || op == '*' || op == '/')
		return true;
	return false;
}

	/*-------------------------------------------*/

bool BrControl(const string &exp, bool print) // �������� ����������� ����������� ������� ������
{
	TStack CtrlTable(ExpSize);
	int pos = 0, errcount = 0, index = 0, temp;
	setlocale(LC_ALL, "Russian");
	while (pos < exp.size())
	{
		if (exp[pos] == '(')
			CtrlTable.Push(++index);
		if (exp[pos] == ')')
			if (CtrlTable.IsEmpty()) // �� ������ '('
			{
				errcount++;
				index++;
				if (print) cout << '-' << ' ' << index << endl;
		}
			else
			{
				index++;
				temp = (int)CtrlTable.Pop();
				if (print) cout << temp << ' ' << index << endl;
			}
		pos++;
	}

	// ���� ���� �� ����, �� ������ �� ������ ������������� ������
	if (!CtrlTable.IsEmpty())
	{
		errcount++;
		temp = (int)CtrlTable.Pop();
		if (print) cout << temp << ' ' << '-' << endl;
	}

	if (errcount == 0)
	{
		if (print) cout << "������ � ����������� ������ ���" << endl;
		return true;
	}
	else
	{
		if (print) cout << "� ����������� ������ ���� ������." << "��������� �� ����� ���� ���������" << endl;
		return false;
	}
}

	/*-------------------------------------------*/

// �������������� ��������� �� ��������� � ����������� �����
// ���������, ��� �������� ����������, ��� ��������

string ConvertToPostfix(const string &InfixExp) // ������������� � ����������� ������
{
	try{
		if (!BrControl(InfixExp, false))
			throw 1;
		char ch;
		string	PrefixExp;
		int pos = 0; // ������ �������� ������� � ���������
		TStack OperationStack(ExpSize);
		while (pos < InfixExp.size())
		{
			ch = InfixExp[pos++];
			if (('0' <= ch) && (ch <= '9') || (ch == '.')) // �������
				PrefixExp += ch;
			if (IsOperation(ch))
			{
				if (OperationStack.IsEmpty()) // ���� ���� ������, �������� �������� � ����� �����
					OperationStack.Push(ch);
				else
				if (GetOpPriority(ch)>GetOpPriority(OperationStack.TopElem()))
					OperationStack.Push(ch);
				else
				{
					while (true)
					{
						PrefixExp += OperationStack.Pop();
						if (GetOpPriority(ch) > GetOpPriority(OperationStack.TopElem()) || OperationStack.IsEmpty())
							break;
					}
					// ��������� � ���� ������� ��������
					OperationStack.Push(ch);
				}
				//���� �� �����, ��������� ������, ����� � ���������� ����� ���� �������� �����
				PrefixExp += ' ';
			}
			if (ch == '(')
				OperationStack.Push(ch);

			if (ch == ')') // � ���� ������ ��������� �� ����� �������� � �������������� ������ ��� ��������,
			{			   // ���� �� �������� ����������� ������. ���� ������ ��� ���� ������������
				while (true)
				{
					if (OperationStack.IsEmpty())
						break;
					if (OperationStack.TopElem() == '(')
					{
						OperationStack.Pop();
						break;
					}
					PrefixExp += OperationStack.Pop();
				}
			}
		}
		while (!OperationStack.IsEmpty()) // ��������� ���������� � ����� ��������, ���� ��� ��������
			PrefixExp += (OperationStack.Pop());
		return PrefixExp;
	}
	catch (int i)
	{
		throw("The expression cannot be calculated");
	}
}

	/*-------------------------------------------*/

double Calculation(const string &exp) // ���������� �������� ���������
{
	int i = 0, j = 0, k = 0;
	TSimpleStackD calc;
	char number[ExpSize] = "";
	double temp, fract;
	string PostFix = ConvertToPostfix(exp);
	while (i < PostFix.size())
	{
		if (('0' <= PostFix[i]) && (PostFix[i] <= '9') || (PostFix[i] == '.')) //�������� �����
			number[j++] = PostFix[i];
		else // ����� ���������
		{
			number[j] = '\0';
			temp = 0.0; fract = 1.0;
			for (k = 0; (number[k] != '\0') && (number[k] != '.'); k++)
				temp = temp * 10.0 + (double)(number[k] - '0');
			if (number[k] == '.')
			{
				for (k = k + 1; number[k] != '\0'; k++)
				{
					fract *= 0.1;
					temp = temp + (double)(number[k] - '0')*fract;
				}
			}
				if (k != 0) // ������� � ���� ������ �� ��������, ������� ���������
					calc.Push(temp); 
				number[0] = '\0'; j = 0; k = 0;
			if (PostFix[i] == ' ') // ������ - �������� ������� ����� �����, ��������� � ��������� �������� �����
			{
				i++;
				continue;
			}
			// ����� 2 �������� �� �����, ��������� ��������� ��������
			temp = calc.Pop();
			switch (PostFix[i])
			{
				case '+': calc.Push(calc.Pop() + temp); break;
				case '-': calc.Push(calc.Pop() - temp); break;
				case '*': calc.Push(calc.Pop() * temp); break;
				case '/': calc.Push(calc.Pop() / temp); break;
			}
		}
		i++;
	}
	// � ����� ��������� ��������� � ����� ����� �������� �������
	return calc.Pop();
}