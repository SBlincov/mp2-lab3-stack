#include "/Library/PrOgA/++proj(autm 2016)/googletest-master/googletest/msvc/simp_stack/3.1/include/TStack.h"
#include <cstdio>
#include "gtest.h"


TEST(TStack, created_stack_is_empty)
//������ ��� ��������� ���� ������
{
	TStack<int> st;

	EXPECT_TRUE(st.IsEmpty());
}

TEST(TStack, created_stack_is_not_nullptr)
//������ ��� ��������� ���� ����� ���� ���� ������
{
	TStack<int> st;

	EXPECT_NE(&st, nullptr);
}

TEST(TStack, stack_in_which_was_inserted_an_element_is_not_empty)
//����, � ������� ������������ ������� ��������, �� ������
{
	TStack<int> st;
	st.Put(0);

	EXPECT_FALSE(st.IsEmpty());
}

TEST(TStack, stack_from_which_was_got_an_element_is_not_full)
//����, �� �������� �������� �������, ��������
{
	TStack<int> st;
	for (int i = 0; i < DefMemSize; ++i)
		st.Put(0);
	st.Get();

	EXPECT_FALSE(st.IsFull());
}

TEST(TStack, last_put_element_is_high_of_stack)
//��������� ����������� ������� - ������� �� ������� �����
{
	TStack<int> st;
	st.Put(0);

	EXPECT_EQ(st.Get(), st.GetHighElem());
}

TEST(TStack, stack_returns_last_put_element)
//���� ���������� ��������� ����������� �������
{
	TStack<int> st;
	st.Put(5);

	EXPECT_EQ(5, st.Get());
}

TEST(TStack, cant_get_from_empty_stack)
//������ ������� ������� �� ������� �����
{
	TStack<int> st;

	EXPECT_ANY_THROW(st.Get());
}

TEST(TStack, can_put_in_stack_with_DefMemSize_size)
//����� �������� �������, ���� ���� �����������������
{
	TStack<int> st;
	for (int i = 0; i < DefMemSize; ++i)
		st.Put(0);

	EXPECT_NO_THROW(st.Put(0));
}
/*
TEST(TStack, stack_is_full_only_when_DataCount_is_equil_DefMemSize_but_it_increases)
//���� �������������� ������ �����, ����� DataCount=DefMemSize
{
	TStack<int> st;
	int result = 0;
	for (int i = 0; i < 4 * DefMemSize; ++i)
	{
		st.Put(i);
		if (st.IsFull())
			result++;
	}

	EXPECT_EQ(result, 4);
}
*/
TEST(TStack, can_copy_stack)
//����� ����������� ����
{
	TStack<int> st1;
	for (int i = 0; i < DefMemSize; ++i)
		st1.Put(0);

	EXPECT_NO_THROW(TStack<int> st2(st1));
}

TEST(TStack, copied_stack_is_equil_to_source_one)
//������������� ���� ����� ���������
{
	TStack<int> st1;
	for (int i = 0; i < DefMemSize; ++i)
		st1.Put(i);
	TStack<int> st2(st1);
	bool result = true;
	for (int i = 0; i < DefMemSize; ++i)
		if (st1.Get() != st2.Get())
		{
			result = false;
			break;
		}

	EXPECT_TRUE(result);
}

TEST(TStack, copied_stack_has_its_own_memory)
//� �������������� ����� ���� �����
{
	TStack<int> st1;
	for (int i = 0; i < DefMemSize; ++i)
		st1.Put(0);
	TStack<int> st2(st1);

	EXPECT_NE(&st1, &st2);
}