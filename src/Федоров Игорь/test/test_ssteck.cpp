#include "/Library/PrOgA/++proj(autm 2016)/googletest-master/googletest/msvc/simp_stack/3.1/include/TSimpleStack.h"
#include <cstdio>
#include "gtest.h"

TEST(TSimpleStack, created_stack_is_empty)
//������ ��� ��������� ���� ������
{
	TSimpleStack<int> st;

	EXPECT_TRUE(st.IsEmpty());
}

TEST(TSimpleStack, created_stack_is_not_nullptr)
//������ ��� ��������� ���� ����� ���� ���� �����
{
	TSimpleStack<int> st;

	EXPECT_NE(&st, nullptr);
}

TEST(TSimpleStack, stack_in_which_was_inserted_an_element_is_not_empty)
//����, � ������� ����������� ������� ��������, �� ������
{
	TSimpleStack<int> st;
	st.Put(0);

	EXPECT_FALSE(st.IsEmpty());
}

TEST(TSimpleStack, stack_from_which_was_got_an_element_is_not_full)
//����, �� �������� �������� �������, ��������
{
	TSimpleStack<int> st;
	for (int i = 0; i < MemSize; ++i)
		st.Put(0);
	st.Get();

	EXPECT_FALSE(st.IsFull());
}

TEST(TSimpleStack, stack_returns_last_put_element)
//���� ���������� ��������� ����������� �������
{
	TSimpleStack<int> st;
	st.Put(5);

	EXPECT_EQ(5, st.Get());
}

TEST(TSimpleStack, cant_get_from_empty_stack)
//����� ������� ������� �� ������� �����
{
	TSimpleStack<int> st;

	EXPECT_ANY_THROW(st.Get());
}

TEST(TSimpleStack, cant_put_in_full_stack)
//����� �������� ������� � ������ ����
{
	TSimpleStack<int> st;
	for (int i = 0; i < MemSize; ++i)
		st.Put(0);

	EXPECT_ANY_THROW(st.Put(0));
}

TEST(TSimpleStack, can_copy_stack)
//����� ����������� ����
{
	TSimpleStack<int> st1;
	for (int i = 0; i < MemSize; ++i)
		st1.Put(0);

	EXPECT_NO_THROW(TSimpleStack<int> st2(st1));
}

TEST(TSimpleStack, copied_stack_is_equil_to_source_one)
//������������� ���� ����� ���������
{
	TSimpleStack<int> st1;
	for (int i = 0; i < MemSize; ++i)
		st1.Put(0);
	TSimpleStack<int> st2(st1);
	bool result = true;
	for (int i = 0; i < MemSize; ++i)
		if (st1.Get() != st2.Get())
		{
			result = false;
			break;
		}

	EXPECT_TRUE(result);
}

TEST(TSimpleStack, copied_stack_has_its_own_memory)
//� �������������� ����� ���� �����
{
	TSimpleStack<int> st1;
	for (int i = 0; i < MemSize; ++i)
		st1.Put(0);
	TSimpleStack<int> st2(st1);

	EXPECT_NE(&st1, &st2);
}