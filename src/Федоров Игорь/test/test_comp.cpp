#include "/Library/PrOgA/++proj(autm 2016)/googletest-master/googletest/msvc/simp_stack/3.1/include/Computere.h"
#include "gtest.h"
#include <cstdio>


TEST(Computer, right_transfom_expression_with_reals)
{
	Computer c("(6+7)*(4+3)-23");
	EXPECT_EQ(2, c.Calculation());
}

TEST(Computer, can_give_bracket_error_in_transformation)
{
	Computer c("7-)(9*6)-(9+3)");
	EXPECT_EQ(false, c.check_brackets());
}
TEST(Computer, right_check_brackets)
{
	Computer c("((2+2)*2)");
	EXPECT_NE(true, c.check_brackets());
}

TEST(Computer, right_transfom_sipmle_expression)
{
	Computer c("2+2*2");
	EXPECT_EQ("2 2 2*+",c.ConversionExpression);
}




TEST(Computer, right_calculate_simple_expression)
{
	Computer c("8+9*6-(1+3)");
	int x = 8 + 9 * 6 - (1 + 3);
	EXPECT_EQ(x, c.Calculation());
}

TEST(Computer, right_calculate_expression_with_reals)
{
	Computer c("(1+2)/(3+4*7)-3*4");
	double x = (1 + 2) / (3 + 4 * 7) - 3 * 4;
	EXPECT_EQ(x, c.Calculation);
}


