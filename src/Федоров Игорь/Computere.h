/*
#pragma comment(lib, "opengl32.lib") 
#pragma comment(lib, "glu32.lib") 
#pragma comment(lib, "glaux.lib")
*/
#include "/Library/PrOgA/++proj(autm 2016)/googletest-master/googletest/msvc/simp_stack/3.1/include/Tstack.h"
#include <iostream>
#include <string>

using namespace std;

class Computer {
private:
	int **table;
	string task, POSTtask;
	int len;
public:
	//constr
	Computer(string str);
	~Computer();
	//metods
	bool check_brackets();
	double Calculation();
//private:
	int get_priorite(char Operation);
	string ConversionExpression();
};