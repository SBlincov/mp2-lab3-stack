#include "arithmetic.h"


arithmetic::arithmetic(string str)
{
	if (IsValid(str))
	{
		infix = str;
		postfix = InfToPost(infix);
		result = (str.size()) ? PostToResult(postfix) : 0;
	}
	else
		throw 1;
}

void arithmetic::PutInfix(string str)
{
	if (IsValid(str))
	{
		infix = str;
		postfix = InfToPost(infix);
		result = (str.size()) ? PostToResult(postfix) : 0;
	}
	else
		throw 1;
}

string arithmetic::InfToPost(string str)
{
	TStack<char> Symbols;
	string post;
	char Buffer;
	int LastPriority = 0, Prrt;
	for (int i = 0; i < str.length(); i++)
	{
		Prrt = PriorityOfChar(str.at(i));
		if (Prrt == 0 || Prrt > LastPriority || (Symbols.IsEmpty() && Prrt != -1))
		{
			Symbols.Put(str.at(i));
			LastPriority = Prrt;
		}
		else if (Prrt != -1)
		{
			if (Prrt == 1)
			{
				Buffer = Symbols.Get();
				while (PriorityOfChar(Buffer) != 0)
				{
					post.push_back(Buffer);
					Buffer = Symbols.Get();
				}
			}
			else
			{
				Buffer = Symbols.Get();
				while (PriorityOfChar(Buffer) >= Prrt && !Symbols.IsEmpty())
				{
					post.push_back(Buffer);
					Buffer = Symbols.Get();
				}
				if (!Symbols.IsEmpty() || PriorityOfChar(Buffer) < Prrt)
					Symbols.Put(Buffer);
				Symbols.Put(str.at(i));
				LastPriority = Prrt;
			}
		}
		if (Prrt == -1)
			post.push_back(str.at(i));
		if (Prrt != -1)
			post.push_back(' ');

	}
	while (!Symbols.IsEmpty()) post.push_back(Symbols.Get());
	return post;
}

int arithmetic::PriorityOfChar(char ch)
{
	for (int i = 0; i < NumOfPriority; i++)
	{
		for (int f = 0; f < priority[i].length(); f++)
			if (priority[i].at(f) == ch) return i;
	}
	return -1;
}

double arithmetic::PostToResult(string str)
{
	TStack<double> res;
	string strtoval;
	double oper1, oper2;
	for (int i = 0; i < str.length(); i++)
	{
		while (PriorityOfChar(str.at(i)) == -1 && str.at(i) != ' ' &&  i < str.length())
			strtoval.push_back(str.at(i++));
		if (!strtoval.empty())
		{
			res.Put(StrToDouble(strtoval));
			strtoval.clear();
		}
		if (PriorityOfChar(str.at(i)) != -1)
		{
			oper2 = res.Get();
			oper1 = res.Get();
			res.Put(Operations(oper1, oper2, str.at(i)));
		}
	}
	return ((res.IsEmpty()) ? 0 : res.Get());
}

double arithmetic::Operations(double num1, double num2, char operation)
{
	switch (operation)
	{
	case '/':
	{
		return num1 / num2;
		break;
	}
	case '*':
	{
		return num1 * num2;
		break;
	}
	case '+':
	{
		return num1 + num2;
		break;
	}
	case '-':
	{
		return num1 - num2;
		break;
	}
	default: 
		throw 2;
	}
}

double arithmetic::StrToDouble(string str)
{
	double result = 0;
	int i;
	bool point = false;
	for (i = 0; i < str.length(); i++)
	{
		if ((str.at(i) == '.') && point) throw 3;
		if (str.at(i) == '.') point = true;
	}
	if (point)
	{
		for (int p = 1, i = (str.find('.') + 1); i < str.length(); i++, p++)
		{
			if (str.at(i) <= '9' && str.at(i) >= '0')
				result += (str.at(i) - '0') * pow(10, -p);
			else
				throw 3;
		}
		for (int p = 0, i = (str.find('.') - 1); i >= 0; i--, p++)
		{
			if (str.at(i) <= '9' && str.at(i) >= '0')
				result += (str.at(i) - '0') * pow(10, p);
			else
				throw 3;
		}
	}
	else
		for (int p = 0, i = str.length() - 1; i >= 0; i--, p++)
		{
			if (str.at(i) <= '9' && str.at(i) >= '0')
				result += (str.at(i) - '0') * pow(10, p);
			else
				throw 3;
		}
	return result;
}

bool arithmetic::IsValid(string str)
{
	bool flag = true;
	int open = 0, close = 0;
	for (int i = 0; (i < str.length()) && flag; i++)
	{
		if (str.at(i) == '(') open++;
		if (str.at(i) == ')') close++;
		if (close > open) flag = false;
	}
	if (close != open) flag = false;
	if (!flag)
	{
		TStack<int> st;
		int numOfErrors = 0, bracketNum = 0;
		cout << "Brackets" << endl;
		cout << "Opening" << " " << "Closing" << endl;
		for (int i = 0; i < str.length(); i++)
		{
			if (str.at(i) == '(')
				 st.Put(++bracketNum);
			if (str.at(i) == ')')
			{
				bracketNum++;
				if (st.IsEmpty())
				{
					numOfErrors++;
					cout << '-' << "       " << bracketNum << endl;
				}
				else
				cout << st.Get() << "       " << bracketNum << endl;
			}
		}
		while (!st.IsEmpty())
		{
			cout << st.Get() << "       " << '-' << endl;
			numOfErrors++;
		}
		cout << "Number of errors: " << numOfErrors << endl;
	}
	return flag;
}