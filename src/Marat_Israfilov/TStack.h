#ifndef __TSTACK_H__
#define __TSTACK_H__

#include "tdataroot.h"

class TStack : public TDataRoot
{
private:
	int Top;
public:
	TStack(int Size = DefMemSize) : TDataRoot(Size) { Top = -1; }
	TStack(const TStack&);
	void  Put(const TData&);
	TData Get(void);
	TData Current_Element() const { return pMem[DataCount - 1]; }
	int Get_Size() { return MemSize; }
	void Print();
	int  IsValid() { return 0; };
};

#endif

