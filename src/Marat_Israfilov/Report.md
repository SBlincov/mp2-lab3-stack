﻿# Методы программирования 2: Стек


## Цели и задачи


 __Цели данной работы__ В рамках лабораторной работы ставится цель разработки двух видов стеков:

* прстейшего, основанного на статическом массиве (класс TSimpleStack);
* более сложного, основанного на использовании динамической структуры (класс TStack).

С помощью разработанных стеков необходимо написать приложение, которое вычисляет арифметическое выражение, заданное в виде строки и вводится пользователем. 
 
 __Задачи:__
 
1. Разработка класса TSimpleStack на основе массива фиксированной длины.
2. Реализация методов класса TDataRoot согласно заданному интерфейсу.
3. Разработка класса TStack, являющегося производным классом от TDataRoot.
4. Разработка тестов для проверки работоспособности стеков.
5. Реализация алгоритма проверки правильности введенного арифметического выражения.
6. Реализация алгоритмов разбора и вычисления арифметического выражения.
7. Обеспечение работоспособности тестов и примера использования.

## Начало работы:

Абстрактный базовый класс `TDataCom` используется в качестве обработчика ошибок для наследуемых классов `TDataRoot` и `TStack`.

```c++
#ifndef __DATACOM_H__
#define __DATACOM_H__

#define DataOK   0
#define DataErr -1

// TDataCom является общим базовым классом
class TDataCom
{
protected:
  int RetCode; // Код завершения

  int SetRetCode(int ret) { return RetCode = ret; }
public:
  TDataCom(): RetCode(DataOK) {}
  virtual ~TDataCom() = 0 {}
  int GetRetCode()
  {
    int temp = RetCode;
    RetCode = DataOK;
    return temp;
  }
};

#endif
```

### 1. класса TSimpleStack:

Этот класс реализован в виде шаблона класса, что позволяет использовать разные типы данных для класса.

```c++
#ifndef __TSIMPLESTACK_H__
#define	__TSIMPLESTACK_H__
#define Size 25

template<class T>
class TSimpleStack
{
private:	
	int Top;
	T Mem[Size];
public:
	TSimpleStack() { Top = -1; };
	TSimpleStack(const TSimpleStack&);
	~TSimpleStack() {};
	bool IsEmpty() const { return Top == -1; };
	bool IsFull() const { return Top == (Size - 1); };
	void Push(const T&);
	T Pop();
	void Print() const;
};

template<class T>
TSimpleStack<T>::TSimpleStack(const TSimpleStack& CopyStack)
{
	Top = CopyStack.Top;
	for (int i = 0; i <= Top; ++i)
	{
		Mem[i] = CopyStack.Mem[i];
	}	
}

template<class T>
void TSimpleStack<T>::Push(const T& Elem)
{
	if (IsFull()) throw 1; //Error: Stack is full!
	Mem[++Top] = Elem;
}

template<class T>
T TSimpleStack<T>::Pop()
{
	if (IsEmpty()) throw 2; //Error: Stack is empty!
	return Mem[Top--];
}

template<class T>
void TSimpleStack<T>::Print() const
{
	if (IsEmpty()) throw 2; //Error: Stack is empty!
	for (int i = 0; i <= Top; ++i)
	{
		cout << Mem[i] << " ";
	}
	cout << endl;
}
#endif
```

### 2. класса TDataRoot:

#### Объявление:

```c++
#ifndef __DATAROOT_H__
#define __DATAROOT_H__

#include "tdatacom.h"

#define DefMemSize   25  // размер памяти по умолчанию

#define DataEmpty  -101  // СД пуста
#define DataFull   -102  // СД переполнена
#define DataNoMem  -103  // нет памяти

typedef double  TElem;    // тип элемента СД
typedef TElem* PTElem;
typedef double  TData;    // тип значений в СД

enum TMemType { MEM_HOLDER, MEM_RENTER };

class TDataRoot: public TDataCom
{
protected:
  PTElem pMem;      // память для СД
  int MemSize;      // размер памяти для СД
  int DataCount;    // количество элементов в СД
  TMemType MemType; // режим управления памятью

  void SetMem(void *p, int Size);             // задание памяти
public:
  virtual ~TDataRoot();
  TDataRoot(int Size = DefMemSize);
  virtual bool IsEmpty(void) const;           // контроль пустоты СД
  virtual bool IsFull (void) const;           // контроль переполнения СД
  virtual void  Put   (const TData &Val) = 0; // добавить значение
  virtual TData Get   (void)             = 0; // извлечь значение

  // служебные методы
  virtual int  IsValid() = 0;                 // тестирование структуры
  virtual void Print()   = 0;                 // печать значений

  // дружественные классы
  friend class TMultiStack;
  friend class TSuperMultiStack;
  friend class TComplexMultiStack;
};

#endif
```

#### Реализация:

```c++
#include "tdataroot.h"

TDataRoot::TDataRoot(int Size) : TDataCom()
{
	if (Size < 0)
		throw SetRetCode(DataNoMem);
	else
	{
		MemSize = Size;
		DataCount = 0;
		if (Size == 0)
		{
			pMem = nullptr;
			MemType = MEM_RENTER;
		}
		else
		{
			pMem = new TElem[MemSize];
			MemType = MEM_HOLDER;
		}
	}
}

TDataRoot::~TDataRoot()
{
	delete[] pMem;	
}

void TDataRoot::SetMem(void *p, int Size)
{
	if (Size < 0) 
		throw SetRetCode(DataNoMem);
	else if(MemType == MEM_HOLDER)
	{		
		MemSize = Size;
		PTElem tmp = new TElem[MemSize];
		p = pMem;
		pMem = tmp;
		tmp = (PTElem)p;
		for (int i = 0; i < DataCount; ++i)		
			pMem[i] = tmp[i];		
		delete[] tmp;
	}
	else
	{	
		MemSize = Size;		
		for (int i = 0; i < DataCount; ++i)		
			((PTElem)p)[i] = pMem[i];	
		pMem = (PTElem)p;		
	}
}

bool TDataRoot::IsEmpty(void) const
{
	return DataCount == 0;
}

bool TDataRoot::IsFull(void) const
{
	return DataCount == MemSize;
}
```

### 3. класса TStack:

#### Объявление:

```c++
#ifndef __TSTACK_H__
#define __TSTACK_H__

#include "tdataroot.h"

class TStack : public TDataRoot
{
private:
	int Top;
public:
	TStack(int Size = DefMemSize) : TDataRoot(Size) { Top = -1; }
	TStack(const TStack&);
	void  Put(const TData&);
	TData Get(void);
	TData Current_Element() const { return pMem[DataCount - 1]; }
	int Get_Size() { return MemSize; }
	void Print();
	int  IsValid() 
	{ 
	int res = 0;
	if (pMem == nullptr)
		res++;
	if (MemSize < DataCount)
		res += 2;
		
	return res;
	}
	
	};

#endif
```

#### Реализация:

```c++
#include "TStack.h"
#include <iostream>
using namespace std;

TStack::TStack(const TStack& CopyStack)
{
	MemSize = CopyStack.MemSize;
	DataCount = CopyStack.DataCount;
	MemType = CopyStack.MemType;
	pMem = new TElem[MemSize];
	for (int i = 0; i < MemSize; ++i)
	{
		pMem[i] = CopyStack.pMem[i];
	}
}

void TStack::Put(const TData& Val)
{
	if (pMem == nullptr) 
	{ 
		throw SetRetCode(DataNoMem); 
	}
	else if (IsFull())
	{
		void* p = nullptr;
		SetMem(p, MemSize + DefMemSize);
		pMem[++Top] = Val;
		DataCount++;
	}
	else
	{
		pMem[++Top] = Val;
		DataCount++;
	}
}

TData TStack::Get(void)
{
	if (pMem == nullptr)	
		throw SetRetCode(DataNoMem);	
	else if (IsEmpty())	
		throw SetRetCode(DataEmpty);	
	else
	{
		DataCount--;
		return pMem[Top--];
	}
}

void TStack::Print()
{
	if (DataCount == 0) { cout << "Stack is empty!"; }
	for (int i = 0; i < DataCount; ++i)
	{
		cout << pMem[i] << " ";
	}
	cout << endl;
}
```

### 4. Тесты:

#### Тесты для класса TSimpleStack:

```c++
#include "TSimpleStack.h"

#include "gtest.h"

TEST(TSimpleStack, created_stack_is_empty)
{
	TSimpleStack<int> st;
	EXPECT_TRUE(st.IsEmpty());
}

TEST(TSimpleStack, can_pop_from_full_stack)
{
	TSimpleStack<int> st;
	for(int i = 0; i < Size; ++i)
		st.Push(0);
	st.Pop();
	EXPECT_FALSE(st.IsFull());
}

TEST(TSimpleStack, can_pop_from_stack)
{
	TSimpleStack<int> st;
	st.Push(0);
	EXPECT_EQ(0, st.Pop());
}

TEST(TSimpleStack, cant_pop_from_empty_stack)
{
	TSimpleStack<int> st;	
	EXPECT_ANY_THROW(st.Pop());
}

TEST(TSimpleStack, can_push_in_stack)
{
	TSimpleStack<int> st;
	st.Push(5);
	EXPECT_FALSE(st.IsEmpty());
}

TEST(TSimpleStack, cant_push_in_full_stack)
{
	TSimpleStack<int> st;
	for (int i = 0; i < Size; ++i)
		st.Push(0);
	EXPECT_ANY_THROW(st.Push(0));
}

TEST(TSimpleStack, can_copy_stack)
{
	TSimpleStack<int> st1;
	for (int i = 0; i < Size; ++i)
		st1.Push(0);
	EXPECT_NO_THROW(TSimpleStack<int> st2(st1));
}

TEST(TSimpleStack, copied_stack_is_equal_to_source_one)
{
	TSimpleStack<int> st1;
	for (int i = 0; i < Size; ++i)
		st1.Push(0);
	TSimpleStack<int> st2(st1);
	bool result = true;
	for (int i = 0; i < Size; ++i)
		if (st1.Pop() != st2.Pop()) { result = false; break; }
	EXPECT_TRUE(result);
}

TEST(TSimpleStack, copied_stack_has_its_own_memory)
{
	TSimpleStack<int> st1;
	for (int i = 0; i < Size; ++i)
		st1.Push(0);
	TSimpleStack<int> st2(st1);	
	EXPECT_NE(&st1, &st2);
}
```

#### Тесты для класса TStack:

```c++
#include "TStack.h"

#include "gtest.h"

TEST(TStack, created_stack_is_empty)
{
	TStack st;
	EXPECT_TRUE(st.IsEmpty());
}

TEST(TStack, can_put_in_stack)
{
	TStack st;
	st.Put(0);
	EXPECT_FALSE(st.IsEmpty());
}

TEST(TStack, can_get_from_stack)
{
	TStack st;
	st.Put(0);
	EXPECT_EQ(0, st.Get());
}

TEST(TStack, current_element_is_displayed_correctly)
{
	TStack st;
	st.Put(0);
	EXPECT_EQ(0, st.Current_Element());
}

TEST(TStack, size_is_displayed_correctly)
{
	TStack st(45);
	EXPECT_EQ(45, st.Get_Size());
}

TEST(TStack, can_get_from_full_stack)
{
	TStack st;
	for (int i = 0; i < st.Get_Size(); ++i)
		st.Put(0);
	st.Get();
	EXPECT_FALSE(st.IsFull());
}

TEST(TStack, cant_get_from_empty_stack)
{
	TStack st;	
	EXPECT_ANY_THROW(st.Get());
}

TEST(TStack, can_put_in_full_stack)
{
	TStack st;	
	for (int i = 0; i < st.Get_Size(); ++i)
		st.Put(0);
	st.Put(0);
	EXPECT_NE(DefMemSize, st.Get_Size());
}

TEST(TStack, can_copy_stack)
{
	TStack st1;
	for (int i = 0; i < st1.Get_Size(); ++i)
		st1.Put(0);
	EXPECT_NO_THROW(TStack st2(st1));
}

TEST(TStack, copied_stack_is_source_one)
{
	TStack st1;
	for (int i = 0; i < st1.Get_Size(); ++i)
		st1.Put(2);
	TStack st2(st1);	
	bool result = true;
	while(!st1.IsEmpty())
		if (st1.Get() != st2.Get()) 
		{ 
			result = false; 
			break; 
		}
	EXPECT_TRUE(result);
}

TEST(TStack, copied_stack_has_its_own_memory)
{
	TStack st1;
	for (int i = 0; i < st1.Get_Size(); ++i)
		st1.Put(0);
	TStack st2(st1);
	EXPECT_NE(&st1, &st2);
}
```

#### Тесты для функций вычисления арифметического выражения arithmetic_expressions:

```c++
#include "TStack.h"
#include "arithmetic_expressions.h"
#include <string>

#include "gtest.h"

TEST(arithmetic_expressions, can_check_symbols)
{
	string str = "(1+2)";
	EXPECT_TRUE(Check_Symbols(str));
}

TEST(arithmetic_expressions, symbols_checked_correctly)
{
	string str = "(1+2f)";
	EXPECT_FALSE(Check_Symbols(str));
}

TEST(arithmetic_expressions, can_check_brackets)
{
	string str = "(1+2)";
	EXPECT_TRUE(Check_Brackets(str));
}

TEST(arithmetic_expressions, brackets_checked_correctly)
{
	string str = "(1+2";
	EXPECT_FALSE(Check_Brackets(str));
}

TEST(arithmetic_expressions, conversion_to_postfix_form_is_correctly)
{
	string str = "(1+2)/(3*4)-5";
	EXPECT_EQ("1 2 +3 4 */5 -", Postfix_Conversion(str));
}

TEST(arithmetic_expressions, calculating_is_correctly)
{
	string str = "(1+2)/(3*4)-5";
	EXPECT_EQ(-4.75, Calculation(str));
}
```

### 5. Алгоритма проверки правильности, разбора и вычисления арифметического выражения:

#### Объявление:

```c++
#ifndef __CONVERT_H__
#define __CONVERT_H__

#include <string>
#include <iostream>
#include "TStack.h"
using namespace std;

bool Check_Symbols(string);
bool Check_Brackets(string);
string Postfix_Conversion(string);
int Operation_Number(char);
bool IsOperation(char);
TData Calculation(string);

#endif
```

#### Реализация:

```c++
#include "arithmetic_expressions.h"

bool Check_Symbols(string InputString)
{
	bool result = true;
	for (string::iterator it = InputString.begin(), end = InputString.end(); it != end; ++it)
	{				
		if (*it >= '0' && *it <= '9' || *it == '.' || *it == ' ' || IsOperation(*it))
		{
			
		}
		else
		{
			result = false;
			break;
		}
	}
	return result;
}

bool Check_Brackets(string InputString) 
{	
	if (!Check_Symbols(InputString)) throw 1; //Error: invalid symbols!
	TStack st;
	int i = 1, errors = 0;
	bool result = true;


	for (string::iterator it = InputString.begin(), end = InputString.end(); it != end; ++it)
	{

		if (*it == '(')
		{
			st.Put((TData)i);
			++i;
		}
		else if (*it == ')')
		{
			if (st.IsEmpty())
			{
				cout << "- " << i << endl;
				++errors;
				result = false;
			}
			else
				cout << (int)st.Get() << " " << i << endl;
			++i;
		}
	}
	while (!st.IsEmpty())
	{
		cout << (int)st.Get() << " -" << endl;
		++errors;
		result = false;
	}
	cout << "Total number of errors = " << errors << endl;
	return result;
}

int Operation_Number(char InChar)
{
	if (InChar == '(')
		return 0;
	if (InChar == ')')
		return 1;
	if (InChar == '+' || InChar == '-')
		return 2;	
	if (InChar == '*' || InChar == '/')
		return 3;	
}

bool IsOperation(char InChar)
{
	if (InChar == '(' || InChar == ')' || InChar == '+' || InChar == '-' || InChar == '*' || InChar == '/')
		return 1;
	else
		return 0;
}

string Postfix_Conversion(string InputString)
{
	if (!Check_Brackets(InputString)) throw 2; //Error: not all brackets!		
	string OutputString;
	TStack tmp;
	int i = 0;

	for (string::iterator it = InputString.begin(), end = InputString.end(); it != end; ++it, ++i)
	{			
		if (IsOperation(*it))
		{
			if (tmp.IsEmpty() || Operation_Number(*it) == 0 || Operation_Number(*it) > Operation_Number(tmp.Current_Element()))
				tmp.Put(*it);
			else if (*it == ')')
			{
				while (tmp.Current_Element() != '(')
					OutputString = OutputString + (char)tmp.Get();
				tmp.Get();
			}
			else
			{
				while (!tmp.IsEmpty() && Operation_Number(*it) <= Operation_Number(tmp.Current_Element()))
				{
					if (tmp.Current_Element() == '(') tmp.Get();
					OutputString = OutputString + (char)tmp.Get();
				}
				tmp.Put(*it);
			}
		}
		else
			if (InputString[i + 1] == '.' || InputString[i] == '.' || (InputString[i + 1] >= '0' && InputString[i + 1] <= '9'))
				OutputString = OutputString + *it;
			else	
				OutputString = OutputString + *it + ' ';
	}	

	while (!tmp.IsEmpty())
		OutputString = OutputString + (char)tmp.Get();	

	return OutputString;
}

TData Calculation(string InputString)
{
	TStack st;
	TData result, tmp1, tmp2;
	string Postfix_Str = Postfix_Conversion(InputString);
	string var = "";

	for (string::iterator it = Postfix_Str.begin(), end = Postfix_Str.end(); it != end; ++it)
	{				
		if (*it >= '0' && *it <= '9' || *it == '.')								
			var += *it;		
		else if(!IsOperation(*it))
		{					
			st.Put(stod(var));
			var = "";			
		}
		else
		{					
			tmp2 = st.Get();
			tmp1 = st.Get();			
			
			if(*it == '+')			
				result = tmp1 + tmp2;
			else if(*it == '-')
				result = tmp1 - tmp2;
			else if (*it == '*')
				result = tmp1 * tmp2;
			else if (*it == '/')
				result = tmp1 / tmp2;		

			st.Put(result);
		}
	}

	return st.Get();
}
```

### 6. Работоспособность тестов и примера использования:

![](http://i82.fastpic.ru/big/2016/1215/e3/b44aeb87a5abc9c592457c74200d04e3.png)

#### Пример использования:

```c++
#include "TStack.h"
#include <iostream>
#include "arithmetic_expressions.h"
using namespace std;

int main()
{
	try
	{	
		string str = "(1+2)/(3+4*6.7)-5.3*4.4";
		cout << "Input string:" << str << endl;
		cout << "Table brackets:" << endl;
		cout << "Answer = " << Calculation(str) << endl;
	}
	catch (int err)
	{
		cout << err << endl;
	}
	return 0;
}
```

![](http://i83.fastpic.ru/big/2016/1215/37/b31511b42c13071e9f641d78be309737.png)


# Вывод:

В данной работе были реализованы два класса:
* __TSimpleStack__ самый простая версия стека на основе статического массива. TSimpleStack был реализован при помощи шаблонов, за счет чего появилась возможность использовать произвольный тип данных используемых в стеке.
* __TStack__ более сложная версия стека, использующая динамическую память, что позволяет стеку увеличивать размер памяти, если элементов оказалось больше чем базовый размер памяти.

Так же были разработанны функции позволяющий проверять, переводить в постфиксную форму и вычислять арифметические выражения, заданные в виде строки символов.

При переводе арифметическое выражение в постфиксную форму, появляется возможность вычислить его за один проход по строке.

Пройденные тесты дают уверенность в том, что классы и фуркции для работы с арифметическими выражениями работают корректно.