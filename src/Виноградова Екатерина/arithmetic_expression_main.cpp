#include <iostream>
#include "tstack.h"
#include "arithmetic_expression.h"

using namespace std;

void main()
{
	string str;
	TData result;

	cout << "Enter arithmetic expression:" << endl;
	cin >> str;
	cout << "Brackets control" << endl;
	result = Calculate(str);
	cout << "Answer:" << result << endl;
}
