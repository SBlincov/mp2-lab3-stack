#ifndef __SIMPLESTACK_H__
#define __SIMPLESTACK_H__

#define DefMemSize 20

class TSimpleStack
{
private:
	int top;
	int data[DefMemSize];
public:
	TSimpleStack() { top = 0; }
	TSimpleStack(const TSimpleStack &st)
	{
		top = st.top;
		for (int i = 0; i < top; i++)
			data[i] = st.data[i];
	}
	bool IsFull() const { return top == DefMemSize; }
	bool IsEmpty() const { return top == 0; }
	void Put(const int val)
	{
		if (IsFull())
			throw "Stack is full";
		data[top++] = val;
	}
	int Get()
	{
		if (IsEmpty())
			throw "Stack is empty";
		return data[--top];
	}
};

#endif