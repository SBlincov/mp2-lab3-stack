#include "tdatstack.h"
#include "simplestack.h"
#include <gtest.h>

using namespace std;

TEST(simpleStack, PutElem_test)
{
	simpleStack<int> st(10);
	st.PutElem(7);
	EXPECT_EQ(7, st.LastElem());
}

TEST(simpleStack, PutElem_in_full_stack)
{
	simpleStack<int> st(1);
	st.PutElem(9);
	EXPECT_ANY_THROW(st.PutElem());
}

TEST(Func_numb, Func_numb_test_1_integer)		//���������� ������ ���� � �����
{
	int i = 0;
	char f[5] = { '1','2','3','4','8' };
	EXPECT_EQ(12348, Func_numb(f, i));
}

TEST(Func_numb, Func_numb_test_2_double)		//���������� ������ ���� � �����
{
	int i = 0;
	char f[5] = { '1','2',',','4','8' };
	EXPECT_EQ(12.48, Func_numb(f, i));
}

TEST(stample_is_good,stample_is_good_Test_1)	//������ ���������� ������
{
	char* p;
	p = new char[8];
	p[0] = '(';
	p[1] = '(';
	p[2] = '4';
	p[3] = '-';
	p[4] = '3';
	p[5] = ')';
	p[6] = '+';
	p[7] = '6';
	p[8] = ')';
	EXPECT_TRUE(stample_is_good(p));
}
	TEST(stample_is_good,stample_is_good_Test_2)//������ ���������� ������
{
	char* p;
	p = new char[8];
	p[0] = ')';
	p[1] = '(';
	p[2] = '4';
	p[3] = '-';
	p[4] = '3';
	p[5] = ')';
	p[6] = '+';
	p[7] = '6';
	p[8] = '(';
	EXPECT_FALSE(stample_is_good(p));
}

TEST(ChrToInt, ChrToInt_Test)
{
	char cr[3] = { '3','5','0' };
	EXPECT_EQ (350, CharToInt(cr));
}

TEST(ChrToDouble, ChrToDouble_Test)
{
	char cr[3] = { '3','5','5' };
	double k;
	k = CharToDouble(cr);
	
	EXPECT_DOUBLE_EQ(0.355, k);
}

TEST(Priority, Priority_Test)	//��������� ��������
{
	char N= brace_1;
	EXPECT_EQ(0, priority(N));
	N = brace_2;
	EXPECT_EQ(1, priority(N));
	N = plus;
	EXPECT_EQ(2, priority(N));
	N = minus;
	EXPECT_EQ(2, priority(N));
	N = mult;
	EXPECT_EQ(3, priority(N));
	N = divide;
	EXPECT_EQ(3, priority(N));
}

TEST(Stack, new_stack_with_negative_length)	//������ ��� ������������� �������� �����
{
	EXPECT_ANY_THROW(Stack<int>(-2));
}

TEST(Stack, new_stack_with_positive_length)	//�������� �����
{
	EXPECT_NO_THROW(Stack<int>(10));
}

TEST(Stack, PutElem_Test)	//���������� ��������
{
	Stack<int> MySt(3);
	int k = 6;
	EXPECT_NO_THROW(MySt.PutElem(k));
	EXPECT_EQ(6, MySt.LastElem());
}


TEST(Stack, PutElem_Test_with_full_stack)	//���������� �������� � �������� �����
{
	Stack<int> MySt(2);
	int k = 6;
	MySt.PutElem(k);
	MySt.PutElem(k);
	MySt.PutElem(k+1);		//�� ������� ������ �������� ������
	EXPECT_EQ(7, MySt.GetElem());
}

TEST(Stack, GetElement_test)		//������� �������
{
	Stack<int> MySt(2);
	int k = 6;
	MySt.PutElem(k);
	k = MySt.GetElem();
	EXPECT_EQ( 6 , k);
}

TEST(Stack, GetElement_from_empty_stack_Test)	//������� ������� �� ������� �����
{
	Stack<int> MySt(2);
	EXPECT_ANY_THROW (MySt.GetElem());
}

TEST(Stack, Last_Element_Test)		//�������� ��������� ������� �����
{
	Stack<int> MySt(2);
	int k = 6;
	MySt.PutElem(k);
	EXPECT_EQ(MySt.LastElem(), 6);
}

TEST(Stack, Get_number_of_last_element_Test)	//����� ���������� ��������(��������� ����� ����)
{
	Stack<int> MySt(2);
	MySt.PutElem(7);
	EXPECT_EQ (0, MySt.GetTop());
}

TEST(Calc, Calc_TEST_1)		//����� �������� ������� ������ ���������
{
	char f[4] = { '4',plus,'5','\0' };
	//4+5
	EXPECT_EQ(9, Calc(f));
}

TEST(Calc, Calc_TEST_2)
{
	char f[10] = { brace_1,brace_1,'3',plus,'2',brace_2,mult,'5',brace_2,'\0' };
	//((3+2)*5)
	EXPECT_EQ(25, Calc(f));
}

TEST(Calc, Calc_TEST_3)
{
	char f[22] = { brace_1,brace_1,'3',plus,brace_1,'2',minus,'1',brace_2,mult,'4',brace_2,divide,'7',plus,brace_1,'4',plus,'5',brace_2,brace_2 ,'\0'};
	// ((3+(2-1)*4)/7+(4+5))
	EXPECT_EQ(10, Calc(f));
}

TEST(Calc, Calc_TEST_4)
{
	char f[5] = { minus, '7',mult,'5', '\0' };
	//-7*5
	EXPECT_EQ(-35, Calc(f));
}
TEST(Calc, Calc_TEST_5_double)
{
	char f[9] = { '7',',','5',plus,'8',',','6','9','\0' };
	//7,5+8,69
	EXPECT_DOUBLE_EQ(16.19, Calc(f));
}
TEST(Calc, Calc_TEST_6_ONE_PROBLEM)
{
	char f[11] = { '1','4',divide ,brace_1,'2',plus,'5',brace_2,mult,'2' };
	//14/(2+5)*2
	EXPECT_DOUBLE_EQ(4.0, Calc(f));
}