#ifndef __SIMPLESTACK_H__
#define __SIMPLESTACK_H__

#define MemSize   255  // ������ ����� �� ���������

template<class MTy>
class TSimpleStack
{
private:
	int top;
	MTy Mem[MemSize];
public:
	TSimpleStack() { top = -1; }
	void Put(const MTy &v);
	MTy Pop();
	bool IsEmpty() const { return top == -1; }
	bool IsFull() const { return top == MemSize - 1; }
};

template <class MTy>
void TSimpleStack<MTy>::Put(const MTy &v)
{
	if (IsFull())
		throw "���� ����������";
	else
	{
		Mem[++top] = v;
	}
}

template <class MTy>
MTy TSimpleStack <MTy> ::Pop()
{
	if (IsEmpty())
		throw "���� ����";
	else
		return Mem[top--];
}

#endif