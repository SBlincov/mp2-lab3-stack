#include "func.h"
#include <cstdio>
#include <gtest\gtest.h>
#include "func.cpp"

TEST(func, CheckSymbols_returns_true_if_string_is_correct)
{
	std::string st = "1+2";

	EXPECT_TRUE(CheckSymbols(st));
}

TEST(func, CheckSymbols_returns_false_if_string_is_incorrect)
{
	std::string st = "1a2";

	EXPECT_FALSE(CheckSymbols(st));
}

TEST(func, CheckBrackets_returns_0_if_string_not_contains_brackets)
{
	std::string st = "1+2";

	EXPECT_EQ(CheckBrackets(st,0),0);
}

TEST(func, CheckBrackets_returns_0_if_string_contains_correct_brackets)
{
	std::string st = "1+((1+2)*1)";

	EXPECT_EQ(CheckBrackets(st, 0), 0);
}

TEST(func, CheckBrackets_returns_not_0_if_string_contains_incorrect_brackets)
{
	std::string st = "(((()())(";

	EXPECT_EQ(CheckBrackets(st, 0), 3);
}

TEST(func, DeleteSpace_deletes_spaces)
{
	std::string st = "1  1";
	DeleteSpace(st);

	EXPECT_EQ(st, "11");
}

TEST(func, CalcBin_calculates_correctly)
{
	EXPECT_EQ(CalcBin(1,2,'-'), -1);
}

TEST(func, CalcPost_calculates_the_example_correctly)
{
	std::string st = "(1+2)/(3+4*6.7)-5.3*4.4";
	double res_st = (1 + 2) / (3 + 4 * 6.7) - 5.3*4.4;
	st = InfiToPost(st);

	EXPECT_TRUE(0.5>=abs(CalcPost(st)-res_st));
}

TEST(func, Caltulate_can_not_calculate_if_string_contains_an_unacceptable_symbol)
{
	std::string st = "1a2";

	EXPECT_ANY_THROW(Calculate(st));
}

TEST(func, Caltulate_can_not_calculate_if_string_contains_incorrect_brackets)
{
	std::string st = "1)((2";

	EXPECT_ANY_THROW(Calculate(st));
}

TEST(func, Calculate_can_calculate_the_example_correctly)
{
	std::string st = "(1+2)/(3+4*6.7)-5.3*4.4";
	double res_st = (1 + 2) / (3 + 4 * 6.7) - 5.3*4.4;

	EXPECT_TRUE(0.5 >= abs(Calculate(st) - res_st));
}