#ifndef __FUNC_CPP__
#define __FUNC_CPP__

//Error 1 "Check your string. The line contains an unacceptable symbol. "
//Error 2 "Check your brackets."

#include "func.h"

bool CheckSymbols(std::string &Line)
{
	
	for (std::string::iterator Line_Iter = Line.begin();Line_Iter != Line.end();++Line_Iter)
		if (GetPrio(*Line_Iter) == -3)
			return false;
	return true;
}

void DeleteSpace(std::string& Line)
{
	for (std::string::iterator Line_Iter = Line.begin();Line_Iter != Line.end();++Line_Iter)
		if (*Line_Iter == ' ')
		{
			Line.erase(Line_Iter);
			Line_Iter--;
		}
}

int CheckBrackets(std::string &Line, bool Do_Print)
{
	TStack<int> Bracket;
	int  Count_Open = 1,  Count_Error = 0, Numb_Bracket;

	for (std::string::iterator Line_Iter = Line.begin();Line_Iter != Line.end();++Line_Iter)
	{
		while ((Line_Iter != Line.end())&&(*Line_Iter != '(') && (*Line_Iter != ')')) //���� �� ���������� ������
			Line_Iter++;
		if (Line_Iter == Line.end())
			break;

		if (*Line_Iter == '(')
			Bracket.Put(Count_Open++);
		if (*Line_Iter == ')')
		{
			if (Do_Print) //���� ���� �������� �� �����
			{
				if (Bracket.IsEmpty())
				{
					Numb_Bracket = 0;
					Count_Error++;
				}
				else
					Numb_Bracket = Bracket.Get();
					printf("%d	%d\n", Numb_Bracket, Count_Open);
			}
			else
				if (Bracket.IsEmpty())
					Count_Error++;
				else
					Bracket.Get();
				
			Count_Open++;
		}
	}

	if (Do_Print) //���� ���� �������� �� �����
		while (Bracket.IsEmpty()==false)
		{
			Count_Error++;
			printf("%d  %d\n", Bracket.Get(), 0);
		}
	else
		while (Bracket.IsEmpty()==false)
		{
			Count_Error++;
			Bracket.Get();
		}

	if (Bracket.GetRetCode()==DataOK)
		return Count_Error;
}

int GetPrio(char Operation)
{
	if (Operation == '(')
		return 0;
	else if (Operation == ')')
		return 1;
	else if ((Operation == '+') || (Operation == '-'))
		return 2;
	else if ((Operation == '*') || (Operation == '/'))
		return 3;
	else if ((Operation >= '0') && (Operation <= '9') || (Operation == '.')) //���� ������ - ������������ �����
		return -1;
	else if (Operation == ' ')
		return -2;
	else //������ ��������� � ������� ASCII � �� ����������� ��������� {'(', ')', '+', '-', '*', '/', '.', ' ', '0'...'9'}
		return -3;
};

std::string InfiToPost(std::string &Line)
{
	TStack<char> Operator;
	std::string Post = "";
	int  High_Prio = -1;
	
	for (std::string::iterator Line_Iter = Line.begin();Line_Iter != Line.end();++Line_Iter)
	{
		while (Line_Iter!=Line.end() && GetPrio(*Line_Iter)==-1) //������ �����
		{
			Post += *Line_Iter;
			Line_Iter++;
		}

		if (Line_Iter == Line.end())
			break;

		if ((*Line_Iter == '(')|| (GetPrio(*Line_Iter) > High_Prio)|| (Operator.IsEmpty()))
		//���� ����������� ������ || ���� �����. ������ �����. ������� || ���� ����
		{
			if (*Line_Iter != '(')
				Post += ' ';
			Operator.Put(*Line_Iter);
			High_Prio = GetPrio(*Line_Iter);
		}
		else if (*Line_Iter == ')')//���� ����������� ������
		{
			while (Operator.GetHighElem() != '(')
				Post += Operator.Get();
			Operator.Get();
		}
		else //���������� �� ����� ���� ��������, �����. ������� <= ���������� �������� ������
		{
			while ((Operator.IsEmpty()==0)&&(GetPrio(*Line_Iter) <= GetPrio(Operator.GetHighElem())))
				Post+= Operator.Get();
			Operator.Put(*Line_Iter);
			Post+= ' ';
		}	
	}
	while (Operator.IsEmpty() == false)//������� �� ����� ��� ��������, ������� ��������
		Post+= Operator.Get();
	Post+= '\0';

	if (Operator.GetRetCode() == DataOK)
		return Post;
};

double CalcBin(double Fir, double Sec, char Op)
{
	if (Op == '-')
		return Fir - Sec;
	else if (Op == '+')
		return Fir + Sec;
	else if (Op == '*')
		return Fir * Sec;
	else if (Op == '/')
		return Fir / Sec;
}

double CalcPost(std::string &Line)
{
	TStack<double> Numbers;
	std::string CurrNumbString = "";
	double CurrNumbDouble = 0;

	for (std::string::iterator Line_Iter = Line.begin();Line_Iter != Line.end();++Line_Iter)
	{
		if (GetPrio(*Line_Iter) == -2)//���� ������
			Line_Iter++;
		while ((Line_Iter != Line.end()) && (GetPrio(*Line_Iter) == -1))//������ ����� �� ������ � �������� ������
		{
			CurrNumbString += *Line_Iter;
			Line_Iter++;
		}
		CurrNumbString += '\0';
		if (CurrNumbString[0] != '\0')//������� �������� ������ � double
			Numbers.Put(stod(CurrNumbString));
		CurrNumbString = "";
		if (GetPrio(*Line_Iter) > 0)//���� ��������
		{
			CurrNumbDouble = CalcBin(Numbers.Get(), Numbers.Get(), *Line_Iter);
			Numbers.Put(CurrNumbDouble);
		}
	}

	if (Numbers.GetRetCode() == DataOK)
		return Numbers.Get();
}

double Calculate(std::string Line)
{
	if (CheckSymbols(Line))
		if (CheckBrackets(Line,0) == 0)
		{
			DeleteSpace(Line);
			return CalcPost(InfiToPost(Line));
		}
		else
			throw 2;//"Check your brackets."
	else
		throw 1;//"Check your string. The line contains an unacceptable symbol."
}
#endif